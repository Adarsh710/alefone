import React from 'react';
import { Route } from 'react-router-dom';
import EquityDashboard from './components/EquityDashboard';
import Advise from "./routes/advise/";

const Equity = ({ match }) => (
  <div>
    <Route exact path={`${match.url}/`} component={EquityDashboard}/>
    <Route exact path={`${match.url}/advise/:oid*`} component={Advise}/>
  </div>
)

export default Equity;
