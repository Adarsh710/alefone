import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Avatar from "@material-ui/core/Avatar";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import InfiniteScroll from "react-infinite-scroll-component";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Share from "@material-ui/icons/Share";

const styles = theme => ({
  card: {
    Width: 100 + "%",
    marginTop: "40px"
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  contains: {
    width: "90%",
    margin: "20px auto"
  },
  full: {
    position: "relative",
    width: 95 + "%",
    margin: "auto",
    marginTop: 10 + "px"
  },
  comment: {
    display: "flex",
    alignItems: "end"
  },
  input: {
    resize: "none",
    boxShadow: "none",
    width: "100%",
    borderRadius: "2px"
  },
  button: {
    margin: theme.spacing.unit
  },
  interact: {
    padding: "0 10px",
    minWidth: 0,
    minHeight: 0
  },
  IMG: {
    width: "17px"
  }
});

class SimpleMediaCard extends React.Component {
  constructor() {
    super();
    this.state = {
      comment: "",
      comments: [],
      hasMore: true,
      isLoading: false,
      oidp: "",
      lastOid: "", //Last comment's oid
      empOid: "",
      isAuth: false
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleDwVotes = this.handleDwVotes.bind(this);
    this.handleReply = this.handleReply.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleUpVotes = this.handleUpVotes.bind(this);
    this.getComments = this.getComments.bind(this);
  }

  componentWillMount() {
    this.state.oidp = window.location.hash.split("/")[4];
    this.getComments();
    this.getEmpProfile();
  }

  handleInputChange = event => {
    this.setState({
      comment: event.target.value.trim()
    });
  };
  handleSubmit = event => {
    console.log("The comment is :", this.state.comment);
    this.setState({ comment: "" });
    this.putComments();
  };
  handleReply = event => {
    console.log("clicked on Reply button");
  };
  handleUpVotes = event => {
    console.log("clicked on like button");
  };
  handleDwVotes = event => {
    console.log("clicked on dislike button");
  };

  getEmpProfile = () => {
    fetch("https://alefone.net/api/v1/employee/profile?alias=ao", {
      method: "GET", // or GET
      headers: {
        authorization: "Token " + localStorage.getItem("currentToken"),
        "Content-Type": "application/json"
      },
      mode: "cors"
    })
      .then(res => res.json())
      .then(res => {
        if (res.result === null) {
          console.log("notAuth");
        }
        this.setState({ empOid: res.result[0].oid, isAuth: true });
      });
  };

  getComments() {
    this.setState({ isLoading: true }, () => {
      fetch(
        `https://alefone.net/api/v1/comment/pages/?alias=ao&oidp=5dc45534421aa912a632d82f&categoryp=mfadvise${
          this.state.lastOid
        }`,
        {
          method: "GET", // or GET
          headers: {
            authorization: "Token " + localStorage.getItem("currentToken"),
            "Content-Type": "application/json"
          },
          mode: "cors"
        }
      )
        .then(res => res.json())
        .then(res => {
          const count = res.count;

          if (res.count === 0) {
            this.setState({ hasMore: false, isLoading: false });
            return;
          }

          console.log(res.result);

          const newComments = res.result.map(data => ({
            name: data.name,
            comment: data.comment,
            upvote: data.numupvote,
            downvote: data.numdownvote,
            time: data.time,
            role: data.role,
            oid: data.oid
          }));

          this.setState({
            comments: [...this.state.comments, ...newComments],
            hasMore: true,
            isLoading: false,
            lastOid: "&oid=" + res.result[count - 1].oid
          });

          console.log("After setSate", this.state.comments);
        })
        .catch(err => {
          console.log("some error happend", err);
          this.setState({ isLoading: false });
        });
    });
  }

  getTime(data) {
    // const months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
    const date = new Date(Number(data));
    return (
      date.getDate() + "/" + (1 + date.getMonth()) + "/" + date.getFullYear()
    );
  }

  putComments() {
    const oidp = "5dc45534421aa912a632d82f",
      category = "mfadvise",
      comment = this.state.comment;
    fetch("https://alefone.net/api/v1/comment?alias=ao&homealias=goodfood", {
      method: "POST", // or POST
      headers: {
        authorization: "Token " + localStorage.getItem("currentToken"),
        "Content-Type": "application/json"
      },
      mode: "cors",
      body: JSON.stringify({
        oidp: oidp,
        categoryp: category,
        text: comment
      })
    })
      .then(res => res.json())
      .then(res => {
        this.getComments();
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    const { comment, comments, hasMore, isLoading } = this.state;
    const { classes } = this.props;
    return (
      <div className="row">
        <div className="col-lg-12 mb-5">
          <Card className={classes.card}>
            <div className="comment-box" style={{ padding: "10px 35px" }}>
              <div className="row">
                <div className="col-lg-10 col-md-10">
                  <textarea
                    name="comment"
                    id=""
                    rows="2"
                    onChange={this.handleInputChange}
                    value={comment}
                    placeholder="Add your comment"
                    className={classes.input}
                  />
                </div>
                <div className="col-lg-2 col-md-2">
                  <Button
                    variant="outlined"
                    size="small"
                    color="primary"
                    style={{
                      margin: "auto",
                      height: "91%",
                      width: "100%"
                    }}
                    className={classes.button}
                    onClick={this.handleSubmit}
                  >
                    Post
                  </Button>
                </div>
              </div>
              <div className="row" style={{ alignItems: "center" }}>
                <div
                  className="col-12"
                  style={{
                    margin: "10px 0"
                  }}
                >
                  <Button
                    className={classes.interact}
                    onClick={this.handleUpVotes}
                  >
                    <img
                      className={classes.IMG}
                      src="assets/images-demo/image-icons/thumb_up-24px.svg"
                      alt=""
                    />
                  </Button>
                  <Button
                    className={classes.interact}
                    onClick={this.handleDwVotes}
                  >
                    <img
                      className={classes.IMG}
                      src="assets/images-demo/image-icons/thumb_up-24px.svg"
                      alt=""
                      style={{ transform: "rotate(180deg)" }}
                    />
                  </Button>
                </div>
              </div>
            </div>

            <InfiniteScroll
              dataLength={comments.length}
              next={this.getComments}
              hasMore={hasMore}
              loader={
                isLoading && <h6 style={{ textAlign: "center" }}>Loading...</h6>
              }
            >
              {comments.map(data => (
                <div className={classes.contains} key={data.time}>
                  <div className={classes.comment}>
                    <Avatar
                      alt="avatar"
                      src="assets/images-demo/g1.jpg"
                      className="rounded-circle header-avatar"
                    />
                    <CardContent
                      style={{
                        padding: "0 15px",
                        paddingTop: 0,
                        paddingBottom: "10px"
                      }}
                    >
                      <Link to="/">{data.name}</Link>
                      <span
                        style={{
                          paddingLeft: "10px",
                          fontSize: ".8rem",
                          color: "darkgray"
                        }}
                      >
                        {this.getTime(data.time)}
                      </span>
                      <Typography>{data.comment}</Typography>
                      <div className="votes" style={{ marginTop: "8px" }}>
                        <Button
                          className={classes.interact}
                          onClick={this.handleUpVotes}
                        >
                          <img
                            className={classes.IMG}
                            src="assets/images-demo/image-icons/thumb_up-24px.svg"
                            alt=""
                          />
                        </Button>

                        <span>{data.upvote}</span>

                        <Button
                          className={classes.interact}
                          onClick={this.handleDwVotes}
                        >
                          <img
                            className={classes.IMG}
                            src="assets/images-demo/image-icons/thumb_up-24px.svg"
                            alt=""
                            style={{ transform: "rotate(180deg)" }}
                          />
                        </Button>

                        <span>{data.downvote}</span>

                        <Button
                          className={classes.interact}
                          style={{ fontSize: "0.75rem" }}
                          onClick={this.handleReply}
                        >
                          Reply
                        </Button>
                      </div>
                    </CardContent>
                  </div>
                </div>
              ))}
            </InfiniteScroll>
          </Card>
        </div>
      </div>
    );
  }
}

SimpleMediaCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SimpleMediaCard);
