import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';
import chartConfig from 'constants/chartConfig';
import React, { Component } from "react";
import QueueAnim from "rc-queue-anim";
import Comments from './Comments';


const line2 = {};
const months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];

line2.options = {
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data: ['Equity', 'Fixed Deposit'],
        textStyle: {
            color: chartConfig.color.text
        }
    },
    toolbox: {
        show: true,
        // feature: {
        //     saveAsImage: { show: true, title: 'save' }
        // }
    },
    calculable: true,
    xAxis: [
        {
            type: 'category',
            boundaryGap: false,
            data: ['Mon.', 'Tue.', 'Wed.', 'Thu.', 'Fri.', 'Sat.', 'Sun.'],
            axisLabel: {
                textStyle: {
                    color: chartConfig.color.text
                }
            },
            splitLine: {
                lineStyle: {
                    color: chartConfig.color.splitLine
                }
            }
        }
    ],
    yAxis: [
        {
            type: 'value',
            axisLabel: {
                textStyle: {
                    color: chartConfig.color.text
                }
            },
            splitLine: {
                lineStyle: {
                    color: chartConfig.color.splitLine
                }
            },
            splitArea: {
                show: true,
                areaStyle: {
                    color: chartConfig.color.splitArea
                }
            }
        }
    ],
    series: [
        {
            name: 'Equity',
            type: 'line',
            stack: 'Sum',
            data: [120, 132, 101, 134, 90, 230, 210]
        },
        {
            name: 'Fixed Deposit',
            type: 'line',
            stack: 'Sum',
            data: [220, 182, 191, 234, 290, 330, 310]
        }
    ]
};


const styles = theme => ({
    card: {
        maxWidth: 400
    }
});

class EquityChart extends Component {
    constructor(props) {
        super(props);

        // Sets up our initial state
        this.state = {
            isLoading: true,
            oid: null,
            chartoptions: line2.options
        };
    }

    componentWillMount() {
        // Loads some users on initial load
        console.log("componentWillMount");
        this.state.oid = window.location.hash.split("/")[4];
        this.loadUsers();
    }

    async getAdiseData() {
        try {
            let equityadvise = await fetch(
                `https://alefone.net/api/v1/summary/equity/${this.state.oid}?alias=ao`,
                {
                    method: "GET", // or GET
                    headers: {
                        authorization: "Token " + localStorage.getItem("currentToken"),
                        "Content-Type": "application/json"
                    },
                    mode: "cors"
                }
            )

            return await equityadvise.json();
        } catch (error) {
            console.log(error);
            return [];
        }
    }

    async getFDData(fdfetchbody) {
        try {
            let fdadvise = await fetch(
                `https://alefone.net/api/v1/summary/wealth/compute/?fd=True`,
                {
                    method: "POST", // or GET
                    headers: {
                        authorization: "Token " + localStorage.getItem("currentToken"),
                        "Content-Type": "application/json"
                    },
                    mode: "cors",
                    body: JSON.stringify(fdfetchbody)
                }
            )
            return await fdadvise.json();
        } catch (error) {
            console.log(error);
            return [];
        }
    }

    getMonthName = (datestring) => {
        let _date = new Date(parseInt(datestring));
        return months[_date.getMonth()];
    }

    loadUsers = () => {
        this.setState({ isLoading: true }, async () => {

            let equityadvisejson = await this.getAdiseData();
            let fdfetchbody = {
                "amount": parseInt(equityadvisejson.result[0].jsondata.metadata.invested),
                "returns": 10.0,
                "duration": parseInt(equityadvisejson.result[0].jsondata.metadata.duration),
                "monthyear": parseInt(equityadvisejson.result[0].jsondata.metadata.startdate)
            };
            let fdadvisejson = await this.getFDData(fdfetchbody);

            let equitydataraw = equityadvisejson.result[0].jsondata.data;
            let fddataraw = fdadvisejson.data;
            let xaxisdata = [];
            let yaxisseriesequitydata = [];
            let yaxisseriesfddata = [];

            for (let i = 0; i < equitydataraw.length; i++) {
                const e = equitydataraw[i];
                const fd = fddataraw[i];
                xaxisdata.push(this.getMonthName(e.equitymonthyear));
                yaxisseriesequitydata.push(e.endingbalance);
                yaxisseriesfddata.push(fd.endingbalance);
            }

            let newchartoptions = Object.assign({}, line2.options);
            newchartoptions.xAxis[0].data = xaxisdata;
            newchartoptions.series[0].data = yaxisseriesequitydata;
            newchartoptions.series[1].data = yaxisseriesfddata;
            this.setState({ isLoading: false, chartoptions: newchartoptions });

            // setTimeout(() => {
            //     fetch(
            //         `https://alefone.net/api/v1/summary/equity/${this.state.oid}?alias=ao`,
            //         {
            //             method: "GET", // or GET
            //             headers: {
            //                 authorization: "Token " + localStorage.getItem("currentToken"),
            //                 "Content-Type": "application/json"
            //             },
            //             mode: "cors"
            //         }
            //     )
            //         .then(res => res.json())
            //         .then(res => {
            //             const count = res.count;
            //             if (res.count == 0) {
            //                 this.setState({ isLoading: false });
            //                 return;
            //             }
            //             //const result = [].concat(...res.result);
            //             console.log(res.result[0]);

            //             let chartdataraw = res.result[0].jsondata.data;
            //             console.log(chartdataraw);
            //             let xaxisdata = [];
            //             let yaxisseriesequitydata = [];

            //             for (let i = 0; i < chartdataraw.length; i++) {
            //                 const d = chartdataraw[i];
            //                 xaxisdata.push(this.getMonthName(d.equitymonthyear));
            //                 yaxisseriesequitydata.push(d.endingbalance)
            //             }

            //             let newchartoptions = Object.assign({}, line2.options);
            //             console.log(newchartoptions);
            //             newchartoptions.xAxis[0].data = xaxisdata;
            //             newchartoptions.series[0].data = yaxisseriesequitydata;
            //             console.log(newchartoptions);
            //             this.setState({ isLoading: false, chartoptions: newchartoptions });

            //             // Merges the next users into our existing users
            //             // this.setState({
            //             //     chartoptions: [...this.state.results, ...newResult]
            //             // });
            //         })
            //         .catch(err => {
            //             this.setState({
            //                 error: err.message,
            //                 isLoading: false
            //             });
            //         });
            // }, 0);
        });
    };

    render() {
        const { chartoptions, isLoading } = this.state;
        const { classes } = this.props;

        return (
            <ReactEcharts option={chartoptions} showLoading={isLoading} theme={"macarons"} />
        );
    }
}

const EquityAdvise = () => (
    <div className="container-fluid no-breadcrumb page-dashboard">
        <QueueAnim type="bottom" className="ui-animate">
            <div key="1">
                <EquityChart classes={styles} />
                <Comments />
            </div>
        </QueueAnim>
    </div>
);

export default EquityAdvise;
