import React, { Component } from "react";
import QueueAnim from "rc-queue-anim";
import { Link } from "react-router-dom";
import InfiniteScroll from "react-infinite-scroll-component";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import red from "@material-ui/core/colors/red";
import Chip from "@material-ui/core/Chip";

import "./styles.scss";

const styles = theme => ({
  card: {
    maxWidth: 400
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  actions: {
    display: "flex"
  },
  expand: {
    transform: "rotate(0deg)",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    }),
    marginLeft: "auto"
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    backgroundColor: red[500]
  },
  chip: {
    margin: theme.spacing.unit
  }
});

class EquityData extends Component {
  constructor(props) {
    super(props);

    // Sets up our initial state
    this.state = {
      error: false,
      hasMore: true,
      isLoading: false,
      results: [],
      lastOid: "",
      count: 0
    };
  }

  componentWillMount() {
    // Loads some users on initial load
    this.loadUsers();
  }

  loadUsers = () => {
    this.setState({ isLoading: true }, () => {
      setTimeout(() => {
        fetch(
          `https://alefone.net/api/v1/summary/equity/pages/?alias=ao${
            this.state.lastOid
          }`,
          {
            method: "GET", // or GET
            headers: {
              authorization: "Token " + localStorage.getItem("currentToken"),
              "Content-Type": "application/json"
            },
            mode: "cors"
          }
        )
          .then(res => res.json())
          .then(res => {
            const count = res.count;
            if (res.count === 0) {
              this.setState({ hasMore: false, isLoading: false });
              return;
            }
            const result = [].concat(...res.result);

            const newResult = result.map(data => ({
              equityName: data.name,
              equitySymbol: data.symbol,
              equityPrice: data.jsondata.metadata.price,
              equityRor: data.ror,
              equityDuration: data.duration,
              equityRisk: data.risk,
              equityDesc: data.description,
              equityRating: data.rating,
              equityOid: data.oid,
              equityShares: data.jsondata.metadata.shares,
              equityAdFirm: data.advisorfirm,
              equityDate: data.jsondata.metadata.time
            }));

            // Merges the next users into our existing users
            this.setState({
              count: res.count,
              hasMore: true,
              isLoading: false,
              lastOid: "&oid=" + res.result[count - 1][0].oid,
              results: [...this.state.results, ...newResult]
            });
          })
          .catch(err => {
            this.setState({
              error: err.message,
              isLoading: false
            });
          });
      }, 10);
    });
  };

  getTime(data){
    const months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
    const date = new Date(Number(data))
    return date.getDay()+" "+months[date.getMonth()]+" "+date.getFullYear()
  }

  render() {
    const { error, hasMore, results } = this.state;
    const { classes } = this.props;

    return (
      <InfiniteScroll
        dataLength={results.length}
        next={this.loadUsers}
        hasMore={hasMore}
        loader={<h6>Loading...</h6>}
      >
        {results.map(user => (
          <div
            key={user.equityOid}
            className="row"
            style={{ width: "100%", margin: "0" }}
          >
            <div className="col-lg-12 mb-5">
              <Card className={classes.card}>
                <CardHeader
                  avatar={
                    <Avatar
                      aria-label="Recipe"
                      className={classes.avatar}
                      style={{ backgroundColor: "#F44336" }}
                    >
                      {user.equityName.split(" ")[0][0]}
                    </Avatar>
                  }
                  action={
                    <Chip
                      label={user.equityRisk}
                      className={classes.chip}
                      style={{ marginRight: "20px", marginTop: "10px" }}
                    />
                  }
                  title={
                    <>
                      <Link
                        to={{
                          pathname: "./equity/advise/"+user.equityOid,
                          state: {
                            fromNotifications: true,
                          },
                          query: {
                            title: "e",
                            name: user.equityName,
                            oid: user.equityOid
                          }
                        }}
                      >
                        {user.equityName}
                      </Link>
                      <Chip
                        label="BUY"
                        className={classes.chip}
                        style={{ marginLeft: "20px" }}
                      />
                    </>
                  }
                  subheader={this.getTime(user.equityDate)}
                />
                <CardContent>
                  <Typography component="p" style={{ marginBottom: "30px" }}>
                    {user.equityDesc}
                  </Typography>
                  <div className="row metrics">
                    <div className="col-xs-6 col-md-4 metric-box">
                      <span className="metric">Price</span>
                      <span className="metric-info">{user.equityPrice}</span>
                    </div>
                    <div className="col-xs-6 col-md-4 metric-box">
                      <span className="metric">Return</span>
                      <span className="metric-info">{user.equityRor}</span>
                    </div>
                    <div className="col-xs-6 col-md-4 metric-box">
                      <span className="metric">Duration</span>
                      <span className="metric-info">
                        {user.equityDuration}
                        Months
                      </span>
                    </div>
                  </div>
                  <div className="row text-start metrics">
                    <div className="col-xs-6 col-md-3 metric-box">
                      <span className="metric">Advisory Firm</span>
                      <span className="metric-info">{user.equityAdFirm}</span>
                    </div>
                  </div>
                </CardContent>
              </Card>
            </div>
          </div>
        ))}
        <hr />
        {error && <div style={{ color: "#900" }}>{error}</div>}
        {!hasMore && (
          <div style={{ textAlign: "center" }}>------No more data------</div>
        )}
      </InfiniteScroll>
    );
  }
}

const EquityDashboard = () => (
  <div className="container-fluid no-breadcrumb page-dashboard">
    <QueueAnim type="bottom" className="ui-animate">
      <div key="1">
        <EquityData classes={styles} />
      </div>
    </QueueAnim>
  </div>
);

export default EquityDashboard;
