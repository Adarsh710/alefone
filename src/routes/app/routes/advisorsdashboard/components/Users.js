import React, { Component } from "react";
import { Link } from "react-router-dom";
import QueueAnim from "rc-queue-anim";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
// import CardContent from "@material-ui/core/CardContent";
import Avatar from "@material-ui/core/Avatar";
import red from "@material-ui/core/colors/red";
import Chip from "@material-ui/core/Chip";

const styles = theme => ({
  card: {
    maxWidth: 400
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  actions: {
    display: "flex"
  },
  expand: {
    transform: "rotate(0deg)",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    }),
    marginLeft: "auto"
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    backgroundColor: red[500]
  },
  chip: {
    margin: theme.spacing.unit
  }
});

class Advisors extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: "",
      advisors: [],
      entid: "",
      count: 0,
      isLoading: true
    };
  }

  componentDidMount() {
    this.getAdvisors();
  }

  componentDidUpdate() {
    let url = window.location.href;
    let entid = url.split("?")[1].split("=")[1];
    if (this.state.entid === entid) {
      return;
    }
    if (this.state.count < 1) {
      this.setState({ advisors: [], isLoading: true });
      this.getAdvisors();
      this.setState({ count: 1 });
    }
  }

  getAdvisors() {
    let url = window.location.href;
    let entid = url.split("?")[1].split("=")[1];
    fetch(
      `https://alefone.net/api/v1/enterprise/users?alias=ao&entid=${entid}&role=advisor`,
      {
        method: "GET", // or GET
        headers: {
          authorization: "Token " + localStorage.getItem("currentToken"),
          "Content-Type": "application/json"
        },
        mode: "cors"
      }
    )
      .then(res => res.json())
      .then(res => {
        const result = [].concat(...res.result);
        const newAdvise = result.map(data => ({
          name: data.name,
          role: data.role,
          status: data.status,
          Oid: data.oid
        }));

        this.setState({
          advisors: [...this.state.advisors, ...newAdvise],
          entid: entid,
          count: 0,
          isLoading: false
        });
      })
      .catch(err => {
        this.setState({ error: err });
      });
  }

  render() {
    const { advisors, error, isLoading } = this.state;
    const { classes } = this.props;

    return (
      <div>
        {isLoading && <div>Loading...</div> }
        {advisors.map(user => (
          <div
            key={user.Oid}
            className="row"
            style={{ width: "100%", margin: "0" }}
          >
            <div className="col-lg-5 mb-5">
              <Card className={classes.card}>
                <CardHeader
                  avatar={
                    <Avatar aria-label="Recipe" className={classes.avatar}>
                      {user.name.charAt(0).toUpperCase()}
                    </Avatar>
                  }
                  title={
                    <React.Fragment>
                      <Link
                        to={{
                          pathname: "/",
                          state: {
                            fromNotifications: true
                          }
                        }}
                      >
                        {user.name}
                      </Link>
                      <Chip
                        label={user.status}
                        className={classes.chip}
                        style={{ marginLeft: "20px" }}
                      />
                    </React.Fragment>
                  }
                  subheader={`${user.role}`}
                />
              </Card>
            </div>
          </div>
        ))}
        <hr />
        {error && <div style={{ color: "#900" }}>{error}</div>}
      </div>
    );
  }
}

const Advisor = () => (
  <div className="container-fluid no-breadcrumb page-dashboard">
    <QueueAnim type="bottom" className="ui-animate">
      <div key="1">
        <Advisors classes={styles} />
      </div>
    </QueueAnim>
  </div>
);

export default Advisor;
