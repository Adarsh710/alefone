import React from 'react';
import { Route } from 'react-router-dom';

import Users from './components/Users';

const Page = ({ match }) => (
    <div>
      <Route path={`${match.url}`} component={Users}/>
    </div>
  )
  
  export default Page;