import React from 'react';
import QueueAnim from 'rc-queue-anim';
import Hero from './AboutAdvisor1';
import './styles.scss';


  const S_User_fname = localStorage.getItem('User_fname');
  console.log("First Name ",S_User_fname);
  const S_User_lname = localStorage.getItem('User_lname');
  console.log("Second Name ",S_User_lname);

  const S_User_enterprise = localStorage.getItem('User_enterprise');
  console.log("Enterprise Name ",S_User_enterprise);

  const S_User_location = localStorage.getItem('User_location');
  console.log("Employee location ",S_User_location); 
  
  const S_User_uid = localStorage.getItem('User_uid');
  console.log("Employee user id ",S_User_uid); 

  const S_User_username = localStorage.getItem('User_username');
  console.log("Employee username ",S_User_username); 
  
  const S_User_gender = localStorage.getItem('User_gender');
  console.log("Employee gender ",S_User_gender); 

  const S_User_dob = localStorage.getItem('User_dob');
  console.log("Employee dob ",S_User_dob); 

  const S_User_status = localStorage.getItem('User_status');
  console.log("Employee status ",S_User_status); 
 



const Culture = () => (
  <article className="article article-bordered section-culture">
   <div className="box-body bg-color-white">
    
    <div className="container-fluid container-mw-xxl">
    <h2 className="section-title mt-0">USER PROFILE</h2>
      <div className="row">
        
        <div className="col-xl-9">
          <div className="row">
            <div className="col-xl-9">
              <p><span className="space-bar bg-primary" /><strong>First Name</strong> : {S_User_fname}</p>
    
              <p><span className="space-bar bg-primary" /><strong>Last Name </strong> :  {S_User_lname}</p>
              <p><span className="space-bar bg-info" /><strong>User id </strong> : {S_User_uid}</p>
              <p><span className="space-bar bg-info" /><strong>Gender </strong> : {S_User_gender}</p>
              <p><span className="space-bar bg-info" /><strong>Username </strong> : {S_User_username}</p>
              <p><span className="space-bar bg-info" /><strong>Dob</strong>: {S_User_dob}</p>
              <p><span className="space-bar bg-danger" /><strong>Enterprise Name</strong> : {S_User_enterprise}</p>
              <p><span className="space-bar bg-danger" /><strong>Location</strong>: {S_User_location} </p>
              <p><span className="space-bar bg-danger" /><strong>Status </strong> : {S_User_status}</p>
            </div>
            
        

          </div>
          <div className="divider my-4" />
          <div className="row">
            <div className="col-xl-4">
            
            </div>
            <div className="col-xl-4">
            
            </div>
            <div className="col-xl-4">
             
            </div>
          </div>
        </div>
      </div>

    </div>
  
    </div>
  </article>
);

const About = () => (
  <section className="page-about chapter">
    <QueueAnim type="bottom" className="ui-animate">
      <div key="1"><Culture /></div>
    </QueueAnim>
  </section>
);

export default About;


