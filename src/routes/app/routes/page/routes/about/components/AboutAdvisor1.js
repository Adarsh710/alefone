
import React from 'react';
import QueueAnim from 'rc-queue-anim';

import './styles.scss';

const Info = () => (
  <div className="container-fluid container-mw-xxl">
    <article className="article">
      <div className="row stat-container">
        <div className="col-md-4">
          <section className="stat-item">
            <span className="stat-num">10</span>
            <span className="stat-desc">Years Experience</span>
          </section>
          <p>Necessitatibus eaque, deleniti error in quam fuga et nisi facere dolore excepturi accusamus dicta reprehenderit dolor. Quo necessitatibus possimus ipsam, nesciunt aspernatur ex libero asperiores, debitis minus nostrum odio. Sunt dolore, dolorem id, iusto quia est unde a doloribus doloremque quisquam repellat nobis enim ipsam eligendi perspiciatis, provident eos aliquid. </p>
        </div>
        <div className="col-md-4">
          <section className="stat-item">
            <span className="stat-num">5K+</span>
            <span className="stat-desc">Satisfied Clients</span>
          </section>
          <p>Tempore adipisci ea accusamus odit consequuntur! Quaerat quos nemo qui ipsam accusantium, nostrum error nesciunt quibusdam velit tempore odit deleniti animi laborum sequi saepe minima atque! Nobis repellendus quos voluptatum sapiente, eveniet aliquid ex eum explicabo soluta delectus officia, dolore aspernatur, dicta nam placeat nostrum aliquam magni? Iste, hic, mollitia.</p>
        </div>
        <div className="col-md-4">
          <section className="stat-item">
            <span className="stat-num">12</span>
            <span className="stat-desc">Kinds of Products</span>
          </section>
          <p>Nostrum, laudantium minima nam dolorum quasi, ut sunt, dolore ratione suscipit sequi vero ducimus earum praesentium odit aut amet voluptates, sint doloribus omnis. Dignissimos, similique neque praesentium mollitia, libero delectus in adipisci ex nihil laborum iure quaerat magnam obcaecati repellendus exercitationem explicabo! Eum, ut voluptatum sapiente dignissimos unde quae eos quis mollitia, voluptate, dolores excepturi? </p>
        </div>
      </div>
    </article>
  </div>
);



  const S_Emp_name = localStorage.getItem('Emp_name');
  console.log("User Name ",S_Emp_name);
  const S_Emp_serial = localStorage.getItem('Emp_serial');
  console.log("User Serial ",S_Emp_serial);

  const S_Emp_enterprise = localStorage.getItem('Emp_enterprise');
  console.log("Enterprise Name ",S_Emp_enterprise);

  const S_Emp_location = localStorage.getItem('Emp_location');
  console.log("Employee location ",S_Emp_location); 
  
  const S_Emp_uid = localStorage.getItem('Emp_uid');
  console.log("Employee user id ",S_Emp_uid); 

  const S_Emp_username = localStorage.getItem('Emp_username');
  console.log("Employee username ",S_Emp_username); 
  
  
  const S_Emp_gender = localStorage.getItem('Emp_gender');
  console.log("Employee gender ",S_Emp_gender); 
 


const Culture = () => (
  <article className="article article-bordered section-culture">
    <div className="container-fluid container-mw-xxl">
      <div className="row">
        <div className="col-xl-3">
          <h2 className="section-title mt-0">ADVISOR PROFILE</h2>
        </div>
        <div className="col-xl-9">
          <div className="row">
            <div className="col-xl-4">
              <p><span className="space-bar bg-primary" /><strong>Name</strong> <br />{S_Emp_name}</p>
            </div>
            <div className="col-xl-4">
              <p><span className="space-bar bg-info" /><strong>Serial No.</strong> <br />{S_Emp_serial}</p>
            </div>
            <div className="col-xl-4">
              <p><span className="space-bar bg-success" /><strong>Employee id </strong> <br />{S_Emp_uid}</p>
            </div>
            <div className="col-xl-4">
              <p><span className="space-bar bg-success" /><strong>Username </strong> <br />{S_Emp_username}</p>
            </div>

            <div className="col-xl-4">
              <p><span className="space-bar bg-success" /><strong>Gender </strong> <br />{S_Emp_gender}</p>
            </div>




          </div>
          <div className="divider my-4" />
          <div className="row">
            <div className="col-xl-4">
              <p><span className="space-bar bg-warning" /><strong>Enterprise Name</strong> <br />{S_Emp_enterprise}</p>
            </div>
            <div className="col-xl-4">
              <p><span className="space-bar bg-danger" /><strong>Location</strong> <br />{S_Emp_location}</p>
            </div>
            <div className="col-xl-4">
              <p><span className="space-bar bg-dark" /><strong>Status </strong> <br />Email Verified</p>
            </div>
          </div>
        </div>
      </div>

    </div>
  </article>
);

const AboutAdvisor1 = () => (
  <section className="page-about chapter">
    <QueueAnim type="bottom" className="ui-animate">
      <div key="1"><Culture /></div>
    </QueueAnim>
  </section>
);

export default AboutAdvisor1;
