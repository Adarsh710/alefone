import React, { Component } from "react";
import QueueAnim from "rc-queue-anim";
import { Link } from "react-router-dom";
import InfiniteScroll from "react-infinite-scroll-component";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import green from "@material-ui/core/colors/green";
import Chip from "@material-ui/core/Chip";

import "./styles.scss";

const styles = theme => ({
  card: {
    maxWidth: 400
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  actions: {
    display: "flex"
  },
  expand: {
    transform: "rotate(0deg)",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    }),
    marginLeft: "auto"
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    backgroundColor: green[500]
  },
  chip: {
    margin: theme.spacing.unit
  }
});

class WealthData extends Component {
  constructor(props) {
    super(props);

    // Sets up our initial state
    this.state = {
      error: false,
      hasMore: true,
      isLoading: false,
      results: [],
      lastOid: "",
      count: 0
    };
  }

  componentWillMount() {
    // Loads some users on initial load
    this.loadUsers();
  }

  loadUsers = () => {
    this.setState({ isLoading: true }, () => {
      setTimeout(() => {
        fetch(
          `https://alefone.net/api/v1/summary/wealth/pages/?alias=ao${
            this.state.lastOid
          }`,
          {
            method: "GET", // or GET
            headers: {
              authorization: "Token " + localStorage.getItem("currentToken"),
              "Content-Type": "application/json"
            },
            mode: "cors"
          }
        )
          .then(res => res.json())
          .then(res => {
            const count = res.count;
            if (res.count === 0) {
              this.setState({ hasMore: false, isLoading: false });
              return;
            }
            const result = [].concat(...res.result);

            const newResult = result.map(data => ({
              wealthName: data.name,
              wealthSymbol: data.symbol,
              wealthPrice: data.jsondata.metadata.invested,
              wealthRor: data.ror,
              wealthDuration: data.duration,
              wealthRisk: data.risk,
              wealthDesc: data.description,
              wealthRating: data.rating,
              wealthOid: data.oid,
              wealthAdFirm: data.advisorfirm,
              wealthDate: data.jsondata.metadata.time
            }));

            // Merges the next users into our existing users
            this.setState({
              count: res.count,
              hasMore: true,
              isLoading: false,
              lastOid: "&oid=" + res.result[count - 1][0].oid,
              results: [...this.state.results, ...newResult]
            });
          })
          .catch(err => {
            this.setState({
              error: err.message,
              isLoading: false
            });
          });
      }, 0);
    });
  };

  getTime(data){
    const months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
    const date = new Date(Number(data))
    return date.getDay()+" "+months[date.getMonth()]+" "+date.getFullYear()
  }

  render() {
    const { error, hasMore, results } = this.state;
    const { classes } = this.props;

    return (
      <InfiniteScroll
        dataLength={results.length}
        next={this.loadUsers}
        hasMore={hasMore}
        loader={<h6 style={{ textAlign: "center" }}>Loading...</h6>}
      >
        {results.map(user => (
          <div
            key={user.wealthOid}
            className="row"
            style={{ width: "100%", margin: "0" }}
          >
            <div className="col-lg-12 mb-5">
              <Card className={classes.card}>
                <CardHeader
                  avatar={
                    <Avatar aria-label="Recipe" className={classes.avatar}>
                      {user.wealthName.split(" ")[0][0]+user.wealthName.split(" ")[1][0]}
                    </Avatar>
                  }
                  action={
                    <Chip
                      label={user.wealthRisk}
                      className={classes.chip}
                      style={{ marginRight: "20px", marginTop: "10px" }}
                    />
                  }
                  title={
                    <>
                      <Link
                        to={{
                          pathname: "./chart/line",
                          state: {
                            fromNotifications: true
                          },
                          query: {
                            title: "w",
                            name: user.wealthName,
                            oid: user.wealthOid
                          }
                        }}
                      >
                        {user.wealthName}
                      </Link>
                      <Chip
                        label="BUY"
                        className={classes.chip}
                        style={{ marginLeft: "20px" }}
                      />
                    </>
                  }
                  subheader={this.getTime(user.wealthDate)}
                />
                <CardContent>
                  <Typography component="p" style={{ marginBottom: "30px" }}>
                    {user.wealthDesc}
                  </Typography>
                  <div className="row text-center metrics">
                    <div className="col-xs-6 col-md-3 metric-box">
                      <span className="metric">Invest</span>
                      <span className="metric-info">Rs {user.wealthPrice}</span>
                    </div>
                    <div className="col-xs-6 col-md-3 metric-box">
                      <span className="metric">Return</span>
                      <span className="metric-info">{user.wealthRor}% p.a.</span>
                    </div>
                    <div className="col-xs-6 col-md-3 metric-box">
                      <span className="metric">Duration</span>
                      <span className="metric-info">
                        {user.wealthDuration}
                        &nbsp;months
                      </span>
                    </div>
                  </div>
                  <div className="row metrics">
                    <div className="col-xs-6 col-md-3 metric-box">
                      <span className="metric-info">{user.wealthAdFirm}</span>
                    </div>
                  </div>
                </CardContent>
              </Card>
            </div>
          </div>
        ))}
        <hr />
        {error && <div style={{ color: "#900" }}>{error}</div>}
        {!hasMore && (
          <div style={{ textAlign: "center" }}>----No More Results----</div>
        )}
      </InfiniteScroll>
    );
  }
}

const WealthDashboard = () => (
  <div className="container-fluid no-breadcrumb page-dashboard">
    <QueueAnim type="bottom" className="ui-animate">
      <div key="1">
        <WealthData classes={styles} />
      </div>
    </QueueAnim>
  </div>
);

export default WealthDashboard;
