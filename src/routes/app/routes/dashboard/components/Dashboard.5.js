import React from 'react';
import QueueAnim from 'rc-queue-anim';
import KPIsChart from './KPIsChart';
import AquisitionChart from './AquisitionChart';
//import StatBoxes from './StatBoxes';
import Investment1 from './Investment1';
import Investment2 from './Investment2';
import Investment3 from './Investment3';
import Investment4 from './Investment4';
import Investment5 from './Investment5';
//import BenchmarkChart from './BenchmarkChart';
import './styles.scss';

const Main = () => (
  <div className="row">
    <div className="col-xl-6">
      <div className="box box-default">
        <div className="box-body">
          <KPIsChart />
        </div>
      </div>
    </div>
    <div className="col-xl-6">
      <div className="box box-default">
        <div className="box-body">
          <AquisitionChart />
        </div>
      </div>
    </div>
  </div>
);


const FirstInvestment = () => (
  <div className="box box-default">
    <div className="box-body">
      <div className="row">
        <div className="col-xl-12">
          <div className="box box-transparent">
            <div className="box-header">Trending Mutual Funds</div>
            <div className="box-body">
              <div className="row text-center metrics">
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">Portfolio Name</span>
                  <span className="metric-info">Mirae Asset Cap Fund</span>
                </div>
                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Return Rate</span>
                  <span className="metric-info">15.6%</span>
                </div>
                <div className="col-xs-6 col-md-1 metric-box">
                  <span className="metric">Risk</span>
                  <span className="metric-info">Medium</span>
                </div>
                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Duration</span>
                  <span className="metric-info">2 Yrs</span>
                </div>

                <div className="col-xs-6 col-md-1 metric-box">
                  <span className="metric">Type</span>
                  <span className="metric-info">Buy</span>
                </div>

                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Rating</span>
                  <span className="metric-info">Top</span>
                </div>
              </div>

              <div className="row text-center comments">
                <div className="col-xs-6 col-md-3 comment-box">
                  <span className="comment-label">Advisor Comments</span>
                  <input id="example" type="text" name="Ntext" size="80"></input>
 
                </div>
                </div>


             
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
);

const SecondInvestment = () => (
  <div className="box box-default">
    <div className="box-body">
      <div className="row">
        <div className="col-xl-12">
          <div className="box box-transparent">
            <div className="box-header">Trending Mutual Funds</div>
            <div className="box-body">
             
              <div className="row text-center metrics">
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">Portfolio Name</span>
                  <span className="metric-info">ICICI Prudential Fund</span>
                </div>
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">3 Yr</span>
                  <span className="metric-info">13.36%</span>
                </div>
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">5 Yrs</span>
                  <span className="metric-info">12.08%</span>
                </div>
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">Type</span>
                  <span className="metric-info">Mid Cap</span>
                </div>
              </div>

              <div className="row text-center comments">
                <div className="col-xs-6 col-md-3 comment-box">
                  <span className="comment-label">Advisor Comments</span>
                  <input id="example" type="text" name="Ntext" size="80"></input>
 
                </div>
                </div>

             
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
);

const ThirdInvestment = () => (
  <div className="box box-default">
    <div className="box-body">
      <div className="row">
        <div className="col-xl-12">
          <div className="box box-transparent">
            <div className="box-header">Trending Mutual Funds</div>
            <div className="box-body">
              <div className="row text-center metrics">
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">Portfolio Name</span>
                  <span className="metric-info">Axis Bluechip Fund</span>
                </div>
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">3 Yr</span>
                  <span className="metric-info">14.47%</span>
                </div>
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">5 Yrs</span>
                  <span className="metric-info">12.48%</span>
                </div>
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">Type</span>
                  <span className="metric-info">Mid cap</span>
                </div>
              </div>

    
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
);

const FourthInvestment = () => (
  <div className="box box-default">
    <div className="box-body">
      <div className="row">
        <div className="col-xl-12">
          <div className="box box-transparent">
            <div className="box-header">Trending Mutual Funds</div>
            <div className="box-body">
              <div className="row text-center metrics">
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">Portfolio Name</span>
                  <span className="metric-info">SBI Magnum Fund</span>
                </div>
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">3 Yr</span>
                  <span className="metric-info">12.32%</span>
                </div>
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">5 Yrs</span>
                  <span className="metric-info">16.07%</span>
                </div>
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">Type</span>
                  <span className="metric-info">Balanced Funds</span>
                </div>
              </div>


            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
);

const FifthInvestment = () => (
  <div className="box box-default">
    <div className="box-body">
      <div className="row">
        <div className="col-xl-12">
          <div className="box box-transparent">
            <div className="box-header">Trending Mutual Funds</div>
            <div className="box-body">
              <div className="row text-center metrics">
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">Portfolio Name</span>
                  <span className="metric-info">SBI Bluechip Fund</span>
                </div>
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">3 Yr</span>
                  <span className="metric-info">9.9%</span>
                </div>
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">5 Yrs</span>
                  <span className="metric-info">13.59%</span>
                </div>
                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric">Type</span>
                  <span className="metric-info">Multicap</span>
                </div>
              </div>

              <div className="row text-center comments">
                <div className="col-xs-6 col-md-3 comment-box">
                  <span className="comment-label">Advisor Comments</span>
                  <input class="center-block" type="text" name="Ntext" align="right" width="80" height="200"></input>
 
                </div>
                </div>

            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
);




const Dashboard = () => (
  <div className="container-fluid no-breadcrumb page-dashboard">

    <QueueAnim type="bottom" className="ui-animate">
      <div key="1"><Main /></div>
      <div key="2"><FirstInvestment/></div>
      <div key="3"><SecondInvestment/></div>
      <div key="4"><ThirdInvestment/></div>
      <div key="5"><FourthInvestment/></div>
      <div key="6"><FifthInvestment/></div>
    </QueueAnim>
  </div>
);


export default Dashboard;
