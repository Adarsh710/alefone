import React from 'react';
import QueueAnim from 'rc-queue-anim';
import KPIsChart from './KPIsChart';
import AquisitionChart from './AquisitionChart';
import Button from '@material-ui/core/Button';
//import StatBoxes from './StatBoxes';
import Investment1 from './Investment1';
import Investment2 from './Investment2';
import Investment3 from './Investment3';
import Investment4 from './Investment4';
import Investment5 from './Investment5';
//import BenchmarkChart from './BenchmarkChart';
import './styles.scss';



const FirstInvestment = () => (
  <div className="box box-default">
    <div className="box-body">
      <div className="row">
        <div className="col-xl-12">
          <div className="box box-transparent">
            <div className="box-header"></div>

            <div className="box-body">
              <div className="row text-center metrics">

                <div className="col-xs-6 col-md-3 metric-box">
                  <span className="metric"> <a href="#/app/chart/bar"> Portfolio Name</a> </span>
                  <span className="metric-info"> <a href="#/app/chart/bar"> Mirae Asset Cap Fund </a>  </span>
                </div>
                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Return Rate</span>
                  <span className="metric-info">15.6%</span>
                </div>
                <div className="col-xs-6 col-md-1 metric-box">
                  <span className="metric">Risk</span>
                  <span className="metric-info">Medium</span>
                </div>
                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Duration</span>
                  <span className="metric-info">12 Months</span>
                </div>

                <div className="col-xs-6 col-md-1 metric-box">
                  <span className="metric">Type</span>
                  <span className="metric-info">Buy</span>
                </div>

                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Rating</span>
                  <span className="metric-info">5</span>
                </div>
              </div>

              <div className="row text-center comments">
                <div className="col-xs-6 col-md-3 comment-box">
                  <textarea Name="content" ROWS="1" COLS="120">A recommended short term investment for stable returns. </textarea>
 
                </div>
                </div>


             
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
);

const SecondInvestment = () => (
  <div className="box box-default">
    <div className="box-body">
      <div className="row">
        <div className="col-xl-12">
          <div className="box box-transparent">
            <div className="box-header"></div>

            <div className="box-body">
              <div className="row text-center metrics">

                <div className="col-xs-6 col-md-3 metric-box">
                <span className="metric"> <a href="#/app/chart/bar"> Portfolio Name</a> </span>
                <span className="metric-info"> <a href="#/app/chart/bar"> Axis BlueChip Fund </a>  </span>
                </div>
                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Return Rate</span>
                  <span className="metric-info">14.3%</span>
                </div>
                <div className="col-xs-6 col-md-1 metric-box">
                  <span className="metric">Risk</span>
                  <span className="metric-info">Medium</span>
                </div>
                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Duration</span>
                  <span className="metric-info">6 Months</span>
                </div>

                <div className="col-xs-6 col-md-1 metric-box">
                  <span className="metric">Type</span>
                  <span className="metric-info">Buy</span>
                </div>

                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Rating</span>
                  <span className="metric-info">5</span>
                </div>
              </div>

              <div className="row text-center comments">
                <div className="col-xs-6 col-md-3 comment-box">
                  <textarea Name="content" ROWS="1" COLS="120">Best investments in the mid cap section with medium risk.</textarea>
 
                </div>
                </div>

             
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
);

const ThirdInvestment = () => (
  <div className="box box-default">
    <div className="box-body">
      <div className="row">
        <div className="col-xl-12">
          <div className="box box-transparent">
            <div className="box-header"></div>

            <div className="box-body">
              <div className="row text-center metrics">

                <div className="col-xs-6 col-md-3 metric-box">
                <span className="metric"> <a href="#/app/chart/bar"> Portfolio Name</a> </span>
                <span className="metric-info"> <a href="#/app/chart/bar"> ICICI Prudential Fund </a>  </span>
                </div>
                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Return Rate</span>
                  <span className="metric-info">11.59%</span>
                </div>
                <div className="col-xs-6 col-md-1 metric-box">
                  <span className="metric">Risk</span>
                  <span className="metric-info">Medium</span>
                </div>
                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Duration</span>
                  <span className="metric-info">24 Months</span>
                </div>

                <div className="col-xs-6 col-md-1 metric-box">
                  <span className="metric">Type</span>
                  <span className="metric-info">Buy</span>
                </div>

                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Rating</span>
                  <span className="metric-info">4.2</span>
                </div>
              </div>

              <div className="row text-center comments">
                <div className="col-xs-6 col-md-3 comment-box">
                  <textarea Name="content" ROWS="1" COLS="120">Great returns in last few years in the mid cap section</textarea>
 
                </div>
                </div>


             
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
);

const FourthInvestment = () => (
  <div className="box box-default">
    <div className="box-body">
      <div className="row">
        <div className="col-xl-12">
          <div className="box box-transparent">
            <div className="box-header"></div>

            <div className="box-body">
              <div className="row text-center metrics">

                <div className="col-xs-6 col-md-3 metric-box">
                <span className="metric"> <a href="#/app/chart/bar"> Portfolio Name</a> </span>
                <span className="metric-info"> <a href="#/app/chart/bar"> SBI BlueChip Fund </a>  </span>
                </div>
                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Return Rate</span>
                  <span className="metric-info">9.31%</span>
                </div>
                <div className="col-xs-6 col-md-1 metric-box">
                  <span className="metric">Risk</span>
                  <span className="metric-info">Low</span>
                </div>
                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Duration</span>
                  <span className="metric-info">8 Months</span>
                </div>

                <div className="col-xs-6 col-md-1 metric-box">
                  <span className="metric">Type</span>
                  <span className="metric-info">Buy</span>
                </div>

                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Rating</span>
                  <span className="metric-info">4.4</span>
                </div>
              </div>

              <div className="row text-center comments">
                <div className="col-xs-6 col-md-3 comment-box">
                  <textarea Name="content" ROWS="1" COLS="120"> Best performing investments in the mid cap section with medium risk</textarea>
 
                </div>
                </div>


             
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
);

const FifthInvestment = () => (
  <div className="box box-default">
    <div className="box-body">
      <div className="row">
        <div className="col-xl-12">
          <div className="box box-transparent">
            <div className="box-header"></div>

            <div className="box-body">
              <div className="row text-center metrics">

                <div className="col-xs-6 col-md-3 metric-box">
                <span className="metric"> <a href="#/app/chart/bar"> Portfolio Name</a> </span>
                  <span className="metric-info"> <a href="#/app/chart/bar"> SBI Magnum Fund </a>  </span>
                </div>
                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Return Rate</span>
                  <span className="metric-info">15.6%</span>
                </div>
                <div className="col-xs-6 col-md-1 metric-box">
                  <span className="metric">Risk</span>
                  <span className="metric-info">High</span>
                </div>
                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Duration</span>
                  <span className="metric-info">10 Months</span>
                </div>

                <div className="col-xs-6 col-md-1 metric-box">
                  <span className="metric">Type</span>
                  <span className="metric-info">Buy</span>
                </div>

                <div className="col-xs-6 col-md-2 metric-box">
                  <span className="metric">Rating</span>
                  <span className="metric-info">5</span>
                </div>
              </div>

              <div className="row text-center comments">
                <div className="col-xs-6 col-md-3 comment-box">
                  <span className="comment-label">Advisor Comments</span>

                  <textarea Name="content" ROWS="1" COLS="120"> A High risk Multicap long term investment opportunity </textarea>
 
                </div>
                </div>


             
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
);




const Dashboard = () => (
  <div className="container-fluid no-breadcrumb page-dashboard">

    <QueueAnim type="bottom" className="ui-animate">
      <div key="1"><FirstInvestment/></div>
      <div key="2"><SecondInvestment/></div>
      <div key="3"><ThirdInvestment/></div>
      <div key="4"><FourthInvestment/></div>
      <div key="5"><FifthInvestment/></div>
    </QueueAnim>
  </div>
);


export default Dashboard;
