import React from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';

const data = [{
  value: 350,
  name: 'PPF',
}, {
  value: 560,
  name: 'MutualFunds'
}, {
  value: 980,
  name: 'Fixed Deposits'
}, {
  value: 760,
  name: 'Equity Funds'
}, {
  value: 320,
  name: 'Insurance'
}];

const pie = {};
pie.options = {
  tooltip: {
    show: true,
    trigger: 'item',
    formatter: '{b}: {c} ({d}%)'
  },
  legend: {
    show: false,
    orient: 'vertical',
    x: 'right',
    data: ['PPF', 'MutualFunds', 'Fixed Deposits', 'Equity Funds', 'Insurance'],
  },
  series: [{
    type: 'pie',
    selectedMode: 'single',
    radius: ['40%', '52%'],
    color: [
      '#EFE04C',
      '#69D361',
      '#47DAB5',
      '#4AC3D6',
      '#5EA1DA',
    ],
    label: {
      normal: {
        position: 'outside',
        formatter: '{b}',
        textStyle: {
          fontSize: 12
        }
      }
    },
    labelLine: {
      normal: {
        show: true
      }
    },
    data,
    markPint: {
      symbol: 'diamond',
      data: [{symbol: 'diamond', }]
    }
  }]
};

const Chart = () => (
  <ReactEcharts style={{height: '400px'}} option={pie.options} showLoading={false} theme={"macarons"} />
);

export default Chart;
