import React from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';
import chartConfig from 'constants/chartConfig';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Dashboard from '../../../../dashboard/components/Dashboard';





let idValue;
let oidValue;
let oidValue2;
let adviseName;

let URL_OidEquityAdviseApi; 
let URL_OidWealthAdviseApi;
let URL_OidApi;


var duration=1;
var investmentName;

var monthData=[];
var investmentData=[];
var endingBalanceData=[];
var MonthNumbers=[];



const bar1 = {};
const bar2 = {};

/*

const Bar = () => (

  <div className="container-fluid no-breadcrumb">
    <Section1 />
    <Section2/>

  </div>
);
*/
//export default Bar;


class Bar extends React.Component{

  constructor(props)
  {
    super(props);
    
      this.state = {

                  bar1options:  
                  {
                          tooltip: 
                          {
                            trigger: 'axis'
                          },
                          legend: 
                          {
                              data: ['Invested', 'Ending Balance'],
                              textStyle: 
                              {
                                color: chartConfig.color.text
                              }
                          },
                          toolbox: 
                          {
                            show: true,
                            feature: 
                            {
                              saveAsImage: {show: true, title: 'save'}
                            }
                          },
                          calculable: true,
                          xAxis: 
                          [
                              {
                                  type: 'category',
                                  barGap: 0,
                                  data: monthData,

                                  axisLabel: 
                                  {
                                        textStyle: 
                                        {
                                          color: chartConfig.color.text
                                        }
                                  },
                                  splitLine: 
                                  {
                                        lineStyle: 
                                        {
                                          color: chartConfig.color.splitLine
                                        }
                                  }
                              }
                          ],
                
                        yAxis: 
                        [
                          {
                              type: 'value',
                              barGap: 0,
                              min: (endingBalanceData[0]-100),
                              max:(endingBalanceData[8]+100),
                              axisLabel: 
                              {
                                  textStyle: 
                                  {
                                    color: chartConfig.color.text
                                  }
                              },
                              splitLine: 
                              {
                                  lineStyle: 
                                  {
                                    color: chartConfig.color.splitLine
                                  }
                              },
                              splitArea: 
                              {
                                show: true,
                                  areaStyle: 
                                  {
                                    color: chartConfig.color.splitArea
                                  }
                              }
                          }
                        ],
                        series: 
                        [
                            {
                                  name: 'Invested',
                                  barGap: 0,
                                  type: 'bar',
                                  data:investmentData,
                                        markPoint: 
                                        {
                                                data: 
                                                [
                                                      {type: 'max', name: 'Max'},
                                                      {type: 'min', name: 'Min'}
                                                ]
                                        },
                                        markLine: 
                                        {
                                                data: 
                                                [
                                                        {type: 'average', name: 'Average'}
                                                ]
                                        }
                              },
                  
                              {
                                          name: 'Ending Balance',
                                          type: 'bar',
                                          barGap: 0,
                                          color: ['#259fa1'],
                                          data: endingBalanceData,
                                          markPoint: {
                                          data: 
                                          [
                                              {name: 'Highest', value: 700, xAxis: 7, yAxis: 100, symbolSize: 50},
                                              {name: 'Lowest', value: 600, xAxis: 11, yAxis: 3}
                                          ]
                              },
                       
                              markLine: 
                              {
                                      data: 
                                      [
                                          {type: 'average', name: 'Average'}
                                      ]
            
                                  }
                                }
                              ]
                            }
        
      }




      this.updateSeries = () => {
        // The chart is updated only with new options.
        this.setState({

            bar1options: 
            {


                  xAxis: 
                  [
                    { data: monthData}
                  ],

                  yAxis: 
                  [
                    {
                      min: (investmentData[1]-100),
                      max:(endingBalanceData[duration-1]+100)
                      
                    }

                  ],

                  series: 
                  [
                    {data:investmentData},
                    {data: endingBalanceData}
                  ]

            }

            
        });



      // CONSTRUCTOR ------------------------------
      }

  // CLASS --------------------------
  }
  
  
  
  componentDidMount() {
    
    idValue= this.props.location.query.title;
    oidValue=this.props.location.query.oid;
    adviseName=this.props.location.query.name;

    console.log(" OId Value 1 ",oidValue);

    oidValue2=this.props.location.query.oid;

    console.log(" Id Value ",idValue);
    console.log(" Name ",adviseName);
    console.log(" OId Value 2 ",oidValue2);
  
    let urlpart='?alias=ao';


    URL_OidEquityAdviseApi = 'https://alefone.net/api/v1/summary/equity/';
    URL_OidWealthAdviseApi = 'https://alefone.net/api/v1/summary/wealth/';

    if (idValue =='w')
    {
      console.log(" wealth detected");

      URL_OidApi= URL_OidWealthAdviseApi+oidValue2+urlpart;
    }
    else if (idValue =='e')
    {
      console.log(" equity detected");

      URL_OidApi= URL_OidEquityAdviseApi+oidValue2+urlpart;
    }
    else
    {
      console.log(" Check id value ");
    }
    
 

   
    console.log(" URL ", URL_OidApi);

    fetch(URL_OidApi, 
      {
        method: 'GET', // or GET
        headers:{
          'authorization':'Token '+ localStorage.getItem('currentToken'),
          'Content-Type': 'application/json'
        },
        mode: 'cors',
    })
    .catch(error => {
        
      console.log("  Data Fetch Failed ");

    })
    .then(res => res.json())
    .then(res => {
      
      
      let equityResult=res.result;
      let equityCount = res.count;

      console.log("Oid Advise Result ---- : ", equityResult);

        
        console.log("the Count: ", equityCount);
        console.log("the Result: ",equityResult);

        duration= equityResult[0].jsondata.data[0].duration;
        console.log("Duration -----  = ", duration);
        localStorage.setItem('duration', duration);

        // Clearing the monthData Array to Zero
        monthData.length = 0;
        
        for(var i=1; i<=duration; i++) 
        {
          
          monthData.push(parseInt(i));
        }
        console.log(" ----   Month on Month Data =   ", monthData);
 

//---------------- NAME --------------------------------
        
        investmentName =equityResult[0].name
        console.log(" Name = ", investmentName);
        //localStorage.setItem('name', equityResult[0].name);

        // Clearing the investmentData Array to Zero
        investmentData.length = 0;

        for(var i=0; i<duration; i++) 
        {
          
          var tmpInvestmentData = equityResult[0].jsondata.data[i].totalinvestment;
          investmentData.push(parseInt(tmpInvestmentData));

          console.log(" Invested Amount  ", i," ", investmentData[i]);
        }
        console.log(" ---- Loop1: Investment Data =   ",investmentData);


        
        // Clearing the endingBalanceData Array to Zero
        endingBalanceData.length = 0;

        for(var i=0; i<duration; i++) 
        {
          

          var tmpEndingBalanceData = equityResult[0].jsondata.data[i].endingbalance;
          endingBalanceData.push(parseInt(tmpEndingBalanceData));
          console.log(" Ending Balance  ", i," ", endingBalanceData[i]);
        }

        console.log(" ---- Loop2:  Ending Balance Data =   ", endingBalanceData);

        
    
        this.updateSeries.bind(this)
   

    
    })



    

  }



      render()
      {
        
        
        return(
          
          <div className="container-fluid no-breadcrumb">
            <div className="row">
              <div className="col-xl-6">

              <div className="box box-default">
                <div className="box-heading">Current Performance </div>
                <div className="box-body">
                  
                  <ReactEcharts option={this.state.bar1options} showLoading={false} theme={"macarons"}  />
                  <button onClick={this.updateSeries.bind(this)}>Load Performance Data</button>
 


              </div>
              </div>

            </div>
            <div className="col-xl-6">

              <div className="box box-default">
                <div className="box-heading">Projected Performance</div>
                <div className="box-body">
                  <ReactEcharts option={this.state.bar1options}   showLoading={false} theme={"macarons"} />
                  <button onClick={this.updateSeries.bind(this)}>Load Performance Data</button>
              </div>
      </div>

    </div>
  </div>
             
             
  <div className="row">
            
            <div className="col-xl-12">
            <div className="box box-default">
                <div className="box-heading"></div>
                    <div className="box-body">
                      <p> Comments </p>
                        <textarea name="content" ROWS="1" COLS="110"  padding-left="200px"> An interesting comparision - helps me understand how my money can grow each month against a classic fixed deposit !!  -Saurabh Shah.  I didnt know we could track our investment to such detail on a month by month basis  !!  -Rakesh Hiren.</textarea>
                        
                    </div>


                  <div className="row">
                    <div className="col-xl-4"> 
                      <Grid container justify="center" alignItems="center">
                      <p><Avatar alt="Likes" src="assets/images-demo/image-icons/like.png" className="avatar" />Like </p>
                      </Grid>
                    </div>

                    <div className="col-xl-4">
                      <Grid container justify="center" alignItems="left">
                      <p><Avatar alt="Likes" src="assets/images-demo/image-icons/comment.png"  />Comment</p>
                      </Grid>
                    </div>

                    <div className="col-xl-4">
                      <Grid container justify="center" alignItems="center">
                      <p><Avatar alt="Likes" src="assets/images-demo/image-icons/Share.png"  />Share</p>
                      </Grid>
                    </div>
                  </div>
                  </div>
            </div>
            </div>
             

          </div>

        );



    }


}

//-----------------------------






//---------------------------



export default Bar;