import React from 'react';
import ReactEcharts from 'echarts-for-react';
import 'echarts/theme/macarons';
import chartConfig from 'constants/chartConfig';

const bar1 = {};
const bar2 = {};
const bar3 = {};
const bar4 = {};
const bar5 = {};

bar1.options = {
  tooltip: {
    trigger: 'axis'
  },
  legend: {
    data: ['FD', 'Mutual Fund'],
    textStyle: {
      color: chartConfig.color.text
    }
  },
  toolbox: {
    show: true,
    feature: {
      saveAsImage: {show: true, title: 'save'}
    }
  },
  calculable: true,
  xAxis: [
    {
      type: 'category',
      data: ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.', 'Aug.', 'Sep.', 'Oct.', 'Nov.', 'Dec.'],
      axisLabel: {
        textStyle: {
          color: chartConfig.color.text
        }
      },
      splitLine: {
        lineStyle: {
          color: chartConfig.color.splitLine
        }
      }
    }
  ],
  yAxis: [
    {
      type: 'value',
      axisLabel: {
        textStyle: {
          color: chartConfig.color.text
        }
      },
      splitLine: {
        lineStyle: {
          color: chartConfig.color.splitLine
        }
      },
      splitArea: {
        show: true,
        areaStyle: {
          color: chartConfig.color.splitArea
        }
      }
    }
  ],
  series: [
    {
      name: 'FD',
      type: 'bar',
      data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3],
      markPoint: {
        data: [
                    {type: 'max', name: 'Max'},
                    {type: 'min', name: 'Min'}
        ]
      },
      markLine: {
        data: [
                    {type: 'average', name: 'Average'}
        ]
      }
    },
    {
      name: 'Mutual Fund',
      type: 'bar',
      data: [2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3],
      markPoint: {
        data: [
          {name: 'Highest', value: 182.2, xAxis: 7, yAxis: 183, symbolSize: 18},
          {name: 'Lowest', value: 2.3, xAxis: 11, yAxis: 3}
        ]
      },
      markLine: {
        data: [
          {type: 'average', name: 'Average'}
        ]
      }
    }
  ]
};
bar2.options = {
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow'
    }
  },
  legend: {
    data: ['Proj Performance', 'Current Performance', 'Fixed Deposit'],
    textStyle: {
      color: chartConfig.color.text
    }
  },
  calculable: true,
  xAxis: [
    {
      type: 'category',
      data: ['Month 1.', 'Month 2', 'Month 3', 'Month 4', 'Month 5', 'Month 6', 'Month 7','Month 8', 'Month 9', 'Month 10', 'Month 11', 'Month 12'],
      axisLabel: {
        textStyle: {
          color: chartConfig.color.text
        }
      },
      splitLine: {
        lineStyle: {
          color: chartConfig.color.splitLine
        }
      }
    }
  ],
  yAxis: [
    {
      type: 'value',
      axisLabel: {
        textStyle: {
          color: chartConfig.color.text
        }
      },
      splitLine: {
        lineStyle: {
          color: chartConfig.color.splitLine
        }
      },
      splitArea: {
        show: true,
        areaStyle: {
          color: chartConfig.color.splitArea
        }
      }
    }
  ],
  series: [
    {
      name: 'Proj Performance',
      type: 'bar',
      data: [320, 332, 301, 334, 390, 330, 320]
    },
    {
      name: 'Current Performance',
      type: 'bar',
      stack: 'Ads',
      data: [120, 132, 101, 134, 90, 230, 210]
    },
    {
      name: 'Fixed Deposit ',
      type: 'bar',
      stack: 'Ads',
      data: [220, 182, 191, 234, 290, 330, 310]
    },
    {
      name: 'Recurring Deposit',
      type: 'bar',
      stack: 'Ads',
      data: [150, 232, 201, 154, 190, 330, 410]
    },
    {
      name: 'Insurance',
      type: 'bar',
      data: [862, 1018, 964, 1026, 1679, 1600, 1570],
      markLine: {
        itemStyle: {
          normal: {
            lineStyle: {
              type: 'dashed'
            }
          }
        },
        data: [
                    [{type: 'min'}, {type: 'max'}]
        ]
      }
    }
   
  ]
};
bar3.options = {
  title: {
    text: 'World Population',
    subtext: 'From the Internet'
  },
  tooltip: {
    trigger: 'axis'
  },
  legend: {
    data: ['2011', '2012'],
    textStyle: {
      color: chartConfig.color.text
    }
  },
  toolbox: {
    show: true,
    feature: {
      saveAsImage: {show: true, title: 'save'}
    }
  },
  calculable: true,
  xAxis: [
    {
      type: 'value',
      boundaryGap: [0, 0.01],
      axisLabel: {
        textStyle: {
          color: chartConfig.color.text
        }
      },
      splitLine: {
        lineStyle: {
          color: chartConfig.color.splitLine
        }
      }
    }
  ],
  yAxis: [
    {
      type: 'category',
      data: ['Brazil', 'Indonesia', 'USA', 'India', 'China', 'World Population (10k)'],
      axisLabel: {
        textStyle: {
          color: chartConfig.color.text
        }
      },
      splitLine: {
        lineStyle: {
          color: chartConfig.color.splitLine
        }
      },
      splitArea: {
        show: true,
        areaStyle: {
          color: chartConfig.color.splitArea
        }
      }
    }
  ],
  series: [
    {
      name: '2011',
      type: 'bar',
      data: [18203, 23489, 29034, 104970, 131744, 630230]
    },
    {
      name: '2012',
      type: 'bar',
      data: [19325, 23438, 31000, 121594, 134141, 681807]
    }
  ]
};
bar4.options = {
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow'
    }
  },
  legend: {
    data: ['Direct', 'Email', 'Affiliate', 'Video Ads', 'Search'],
    textStyle: {
      color: chartConfig.color.text
    }
  },
  toolbox: {
    show: true,
    feature: {
      saveAsImage: {show: true, title: 'save'}
    }
  },
  calculable: true,
  xAxis: [
    {
      type: 'value',
      axisLabel: {
        textStyle: {
          color: chartConfig.color.text
        }
      },
      splitLine: {
        lineStyle: {
          color: chartConfig.color.splitLine
        }
      }
    }
  ],
  yAxis: [
    {
      type: 'category',
      data: ['Month 1.', 'Month 2', 'Month 3', 'Month 4', 'Month 5', 'Month 6', 'Month 7','Month 8', 'Month 9', 'Month 10', 'Month 11','Month 12'],
      axisLabel: {
        textStyle: {
          color: chartConfig.color.text
        }
      },
      splitLine: {
        lineStyle: {
          color: chartConfig.color.splitLine
        }
      },
      splitArea: {
        show: true,
        areaStyle: {
          color: chartConfig.color.splitArea
        }
      }
    }
  ],
  series: [
    {
      name: 'Direct',
      type: 'bar',
      stack: 'Sum',
      itemStyle: { normal: {label: {show: true, position: 'insideRight'}}},
      data: [320, 302, 301, 334, 390, 330, 320]
    },
    {
      name: 'Email',
      type: 'bar',
      stack: 'Sum',
      itemStyle: { normal: {label: {show: true, position: 'insideRight'}}},
      data: [120, 132, 101, 134, 90, 230, 210]
    },
    {
      name: 'Affiliate',
      type: 'bar',
      stack: 'Sum',
      itemStyle: { normal: {label: {show: true, position: 'insideRight'}}},
      data: [220, 182, 191, 234, 290, 330, 310]
    },
    {
      name: 'Video Ads',
      type: 'bar',
      stack: 'Sum',
      itemStyle: { normal: {label: {show: true, position: 'insideRight'}}},
      data: [150, 212, 201, 154, 190, 330, 410]
    },
    {
      name: 'Search',
      type: 'bar',
      stack: 'Sum',
      itemStyle: { normal: {label: {show: true, position: 'insideRight'}}},
      data: [820, 832, 901, 934, 1290, 1330, 1320]
    }
  ]
};
bar5.options = {
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow'
    }
  },
  legend: {
    data: ['Profit', 'Cost', 'Income'],
    textStyle: {
      color: chartConfig.color.text
    }
  },
  toolbox: {
    show: true,
    feature: {
      saveAsImage: {show: true, title: 'save'}
    }
  },
  calculable: true,
  xAxis: [
    {
      type: 'value',
      axisLabel: {
        textStyle: {
          color: chartConfig.color.text
        }
      },
      splitLine: {
        lineStyle: {
          color: chartConfig.color.splitLine
        }
      }
    }
  ],
  yAxis: [
    {
      type: 'category',
      axisTick: {show: false},
      data: ['Month 1.', 'Month 2', 'Month 3', 'Month 4', 'Month 5', 'Month 6', 'Month 7','Month 8', 'Month 9', 'Month 10', 'Month 11', 'Month 12'],
      axisLabel: {
        textStyle: {
          color: chartConfig.color.text
        }
      },
      splitLine: {
        lineStyle: {
          color: chartConfig.color.splitLine
        }
      },
      splitArea: {
        show: true,
        areaStyle: {
          color: chartConfig.color.splitArea
        }
      }
    }
  ],
  series: [
    {
      name: 'Profit',
      type: 'bar',
      itemStyle: { normal: {label: {show: true, position: 'inside'}}},
      data: [200, 170, 240, 244, 200, 220, 210]
    },
    {
      name: 'Income',
      type: 'bar',
      stack: 'Sum',
      barWidth: 5,
      itemStyle: {normal: {
        label: {show: true}
      }},
      data: [320, 302, 341, 374, 390, 450, 420]
    },
    {
      name: 'Cost',
      type: 'bar',
      stack: 'Sum',
      itemStyle: {normal: {
        label: {show: true, position: 'left'}
      }},
      data: [-120, -132, -101, -134, -190, -230, -210]
    }
  ]
};

const Section1 = () => (
  <div className="row">
    <div className="col-xl-6">

      <div className="box box-default">
        <div className="box-heading">Current Performance</div>
        <div className="box-body">
          <ReactEcharts option={bar1.options} showLoading={false} theme={"macarons"} />
        </div>
      </div>

    </div>
    <div className="col-xl-6">

      <div className="box box-default">
        <div className="box-heading">Projected Performance</div>
        <div className="box-body">
          <ReactEcharts option={bar2.options} showLoading={false} theme={"macarons"} />
        </div>
      </div>

    </div>
  </div>
);

const Section2 = () => (
    

    <div className="row text-center comments">
    
    <div className="col-xs-6 col-md-3 comment-box">
    <textarea name="content" ROWS="3" COLS="125" > A great investment for growth and tax savings - Saurabh Rishabh! </textarea>

    </div>
    </div>
);

/*
const Section2 = () => (
  <div className="row">
    <div className="col-xl-6">

      <div className="box box-default">
        <div className="box-heading">Basic Bar</div>
        <div className="box-body">
          <ReactEcharts option={bar3.options} showLoading={false} theme={"macarons"} />
        </div>
      </div>

    </div>
    <div className="col-xl-6">

      <div className="box box-default">
        <div className="box-heading">Stacked Bar</div>
        <div className="box-body">
          <ReactEcharts option={bar4.options} showLoading={false} theme={"macarons"} />
        </div>
      </div>

    </div>
  </div>
);

const Section3 = () => (
  <div className="row">
    <div className="col-xl-12">

      <div className="box box-default">
        <div className="box-heading">Tornado</div>
        <div className="box-body">
          <ReactEcharts option={bar5.options} showLoading={false} theme={"macarons"} />
        </div>
      </div>

    </div>
  </div>
);
*/
const Bar = () => (
  <div className="container-fluid no-breadcrumb">
    <Section1 />
    <Section2/>

  </div>
);

export default Bar;
