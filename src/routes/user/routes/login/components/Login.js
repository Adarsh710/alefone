import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import APPCONFIG from "constants/appConfig";
import QueueAnim from "rc-queue-anim";
import DEMO from "constants/demoData";
import Swal from "sweetalert2";
import "sweetalert2/src/variables.scss";
import "sweetalert2/src/sweetalert2.scss";

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      brand: APPCONFIG.brand,
      email: "", //b@b.com
      password: "", //123qweasd
      name: "",
      body: "",
      id: "",
      postId: "",
      alert: null
    };
  }

  // Error Catching
  componentDidCatch(error, info) {
    // Something happened to one of my children.
    // Add error to state
    this.setState({
      error: error,
      info: info
    });
  }

  login() {}

  // --------------------------------- Adding Modal Confirmation Box -------------------------

  //-------------------------------    Modal Confirmation Box         ---------------------------

  checkStatus(res) {
    if (res.status >= 200 && res.status < 300) {
      // console.log("Login successful");
      return res;
    } else {
      let err = new Error(res.statusText);
      err.response = res;
      throw err;
    }
  }

  handleCloseAlert = () => {
    this.setState({
      showAlert: false
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    // console.log(this.state);
    // Validating User using indicative packaging
    // Take the input data from state
    const data = this.state;
    const rules = {
      email: "required|email",
      password: "required|string|min:6|confirmed"
    };

    let emailId = this.state.email;
    let pass = this.state.password;
    let token = null;
    let Storedtoken = null;
    let URL_userApi;

    // console.warn("Entered email Id is " + emailId);
    // console.warn("Entered password is " + pass);
    // console.log("  Entered In Login Function   ");

    // This is Dummy url.

    const URL_loginApi = "https://alefone.net/api/v1/login/";

    const URL_adProfileApi =
      "https://alefone.net/api/v1/advisor/profile?alias=ao";

    const URL_empProfileApi =
      "https://alefone.net/api/v1/employee/profile?alias=ao";

    //const SweetAlert = require('react-bootstrap-sweetalert');

    fetch(URL_loginApi, {
      method: "POST",
      mode: "cors",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },

      body: JSON.stringify({
        email: emailId,
        password: pass
      })
    })
      .catch(error => {
        alert(" Incorrect Email or Password !");
        // console.log(" Incorrect Email or Password ");
      })
      .then(res => res.json())
      .then(res => {
        token = res.token;

        let group = res.group;

        // console.log("the token: ", token);
        // console.log("the group: ", group);

        let name;

        JSON.parse(JSON.stringify(group), (key, value) => {
          if (key === "id") {
            // console.log(value);
          }

          if (key === "name") {
            // console.log(value);
            name = value;
            // console.log("Type of user ", name);

            if (name === "employee") {
              URL_userApi = URL_empProfileApi;
              // console.log("User Api - Employee");
            } else {
              URL_userApi = URL_adProfileApi;
              // console.log("User Api - Advisor");
            }
          }
          return value;
        });

        // Storing the Current User Name
        localStorage.setItem(
          "currentUser",
          JSON.stringify({ name: name, token: token })
        );
        // Storing the Current token
        localStorage.setItem("currentToken", token);

        const StoredUser = localStorage.getItem("currentUser");
        const Storedtoken = localStorage.getItem("currentToken");
        // console.log("Name from storage =", StoredUser);
        // console.log("Token from storage =", Storedtoken);

        if (Storedtoken !== null) {
          // console.log(" ----Page redirect successfull------");

          //this.setState({
          // showAlert: true
          //})

          Swal.fire({
            title: "Disclaimer: For Informational Purposes Only  ",
            text:
              " The information contained in our website, services and/or products is for informational purposes only, and is made available to you as help tools for your own use.  Personal Responsibility: We aim to accurately represent the information provided on our website, services, and products. You are acknowledging that you are participating voluntarily in using our website, services, and/or products, and you alone are solely and personally responsible for your results. No Guarantees: Our role is to support and assist you in reaching your goals, but your success depends primarily on your own effort, motivation, commitment and follow-through. We cannot and do not guarantee that you will attain a particular result, and you accept the risk that results differ by each individual.  Assumption of Risk: You agree that you are using your own judgement in using the information provided on and through AlefOne’s Website, Product, and/or Service, which is done at your own risk.  It shall by your own personal responsibility to discern the risks of using our Website, Products, and/or Services.  There are sometimes unknown individual risks and circumstances that can arise during use of our website, services and/or products that cannot be foreseen that can influence or reduce results.  We are not responsible for your personal actions or choices before, during or after any of our services and/or products. You understand that any mention of any product, suggestion, or recommendation is to be taken at your own risk, with no liability on our part. You accept full responsibility and consequences for your use, or non-use, of any information provided by us through any means whatsoever. Your use, or non-use, of this information is at your own risk, and you absolve us of any liability or loss that you, or your family or children (if applicable) or any other person, may incur from your or their use or non-use of the information provided.  No Warranties:  WE MAKE NO REPRESENTATIONS OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, AS TO THE OPERATION OF THIS WEBSITE, THE INFORMATION, CONTENT, MATERIALS, SERVICES, OR PRODUCTS INCLUDED ON THE WEBSITE. TO THE FULL EXTENT PERMISSIBLE BY APPLICABLE LAW, WE DISCLAIM ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. WE WILL NOT BE LIABLE FOR ANY DAMAGES OF ANY KIND ARISING FROM THE USE OF THIS WEBSITE INCLUDING, BUT NOT LIMITED TO, DIRECT, INDIRECT, INCIDENTAL, EQUITABLE, PUNITIVE AND/OR CONSEQUENTIAL DAMAGES.   By using our website, services, and/or products you accept all our terms, conditions and also our privacy policy mentioned in https://alefone.com/privacy.  If you have any questions about this Disclaimer, please contact us at support@alefone.com.  Thank you",
            type: "warning",
            width: "auto",
            height: "auto",
            padding: "5em",
            border: "2px",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "I Accept!",
            cancelButtonText: "I Decline!"
          }).then(result => {
            if (result.value) {
              window.location = "#/app/wdashboard";
            }
          });

          // window.open("#/app/dashboard")
        } else {
          // console.log("Incorrect Email or Password");
          alert(" Incorrect Email or Password !");
        }

        // Get user profile

        // fetch(URL_empProfileApi, {
        //   method: "GET", // or GET
        //   headers: {
        //     authorization: "Token " + localStorage.getItem("currentToken"),
        //     "Content-Type": "application/json"
        //   },
        //   mode: "cors"
        // })
        //   .catch(error => {
        //     // console.log(" User Data Fetch Failed ");
        //   })
        //   .then(res => {
        //     let theResult = res.result;

        //     // console.log("User Profile Result: ", theResult);
        //     if(theResult == null){
        //       return
        //     }
        //     let name;

        //     JSON.parse(JSON.stringify(theResult), (key, value) => {
        //       if (key === "name") {
        //         // console.log(value);
        //         localStorage.setItem("User_fname", value);
        //       }

        //       if (key === "sn") {
        //         // console.log("SN: ", value);
        //         localStorage.setItem("User_lname", value);
        //       }

        //       if (key === "enterprise") {
        //         // console.log("Enterprise name: ", value);
        //         localStorage.setItem("User_enterprise", value);
        //       }
        //       if (key === "location") {
        //         // console.log("location: ", value);
        //         localStorage.setItem("User_location", value);
        //       }
        //       if (key === "uid") {
        //         // console.log("uid: ", value);
        //         localStorage.setItem("User_uid", value);
        //       }

        //       if (key === "user_name") {
        //         // console.log("user_name : ", value);
        //         localStorage.setItem("User_username", value);
        //       }
        //       if (key === "gender") {
        //         // console.log("gender: ", value);
        //         localStorage.setItem("User_gender", value);
        //       }
        //       if (key === "dob") {
        //         // console.log("dob: ", value);
        //         localStorage.setItem("User_dob", value);
        //       }
        //       if (key === "status") {
        //         // console.log("status: ", value);
        //         localStorage.setItem("User_status", value);
        //       }
        //     });
        //   });
      });

    // Displaying custom messages
    const messages = {
      required: "This {{ field }} is required",
      "email.email": "The email is invalid",
      "password.confirmed": "The password is incorrect"
    };
  };

  handleInputChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  render() {
    if (this.state.error) {
      // Some error was thrown. Let's display something helpful to the user
      return (
        <div>
          <h5>Sorry. More than five characters!</h5>
          <details style={{ whiteSpace: "pre-wrap" }}>
            {this.state.info.componentStack}
          </details>
        </div>
      );
    }

    return (
      <div className="body-inner">
        <div className="card card-white">
          <div className="card-content">
            <section className="logo text-center">
              <h1>
                <a href="#/">{this.state.brand}</a>
              </h1>
            </section>

            <form
              className="form-type-material"
              onSubmit={this.handleSubmit}
              noValidate
            >
              <div className="form-group">
                <TextField
                  label="Email"
                  type="email"
                  name="email"
                  onChange={this.handleInputChange}
                  fullWidth
                />
              </div>
              <div className="form-group">
                <TextField
                  label="Password"
                  type="password"
                  name="password"
                  onChange={this.handleInputChange}
                  className="mt-3"
                  fullWidth
                />
              </div>
              <br />

              <button
                className="btn btn-bold btn-block btn-primary"
                type="submit"
              >
                Login
              </button>
            </form>
          </div>
          <div className="card-action border-0 text-right" />
        </div>

        <div className="additional-info">
          <a href={DEMO.forgotPassword}>Forgot your password?</a>
        </div>
      </div>
    );
  }
}

const Page = () => (
  <div className="page-login">
    <div className="main-body">
      <QueueAnim type="bottom" className="ui-animate">
        <div key="1">
          <Login />
        </div>
      </QueueAnim>
    </div>
  </div>
);

export default Page;
