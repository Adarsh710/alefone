import React from 'react';

import APPCONFIG from 'constants/appConfig';
import TextField from '@material-ui/core/TextField';
import QueueAnim from 'rc-queue-anim';
import DEMO from 'constants/demoData';



class Login extends React.Component 
{
  constructor (props) {  
      super(props);
        this.state = {
          brand: APPCONFIG.brand,
          email: '',
          password: '',
          name: '',
          body: '',
          id: '',
          postId: '',

        };
        
        
    };

    // Error Catching
    componentDidCatch(error, info) {
      // Something happened to one of my children.
      // Add error to state
      this.setState({
        error: error,
        info: info,
      });
    }



login () 
{
    let emailId = this.state.email;
    let pass = this.state.password;
    console.warn("email Id is " + emailId);
    console.warn("password is " + pass);

    console.log(" In Login Function")

    if (emailId === '' || pass === '') {
      alert('Enter emailId and Password');
    }
  
    const URL_loginApi = 'https://alefone.net/api/v1/login/'

    const URL_adProfileApi = 'https://alefone.net/api/v1/advisor/profile?alias=ao'

    const URL_empProfileApi = 'https://alefone.net/api/v1/employee/profile?alias=ao'

    const URL_EquityAdviseApi = 'https://alefone.net/api/v1/summary/equity/pages/?alias=ao'

    const URL_WealthAdviseApi = 'https://alefone.net/api/v1/summary/wealth/pages/?alias=ao'

    let token;

    fetch(URL_loginApi,
    {
      method: 'POST',
      method: 'OPTIONS',
      mode: 'cors',
      headers: 
      {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        
      },

      body: JSON.stringify(
        {
        'email': emailId,
        'password': pass
        })

    })
    
    .then(res => res.json())
    
    .then(res => {
      
      token = res.token;

      let group=res.group;

      console.log("the token: ", token);
      console.log("the group: ", group);


    
      let name;

      JSON.parse(JSON.stringify(group),(key,value)=>
      {
          if (key === 'id') {
            console.log(value);
          }
        
          if (key === 'name') {
          
            console.log(value);
            name =value;
            console.log("name", name);

          }
        return value;

      });
      
      // Storing the Current User Name
      localStorage.setItem('currentUser', JSON.stringify({ name: name,token: token }));
      // Storing the Current token
      localStorage.setItem('currentToken', token);

      const StoredUser = localStorage.getItem('currentUser');
      const Storedtoken = localStorage.getItem('currentToken');
      console.log("Name from storage =", StoredUser);
      console.log("Token from storage =", Storedtoken);


      // Get user profile

      fetch(URL_empProfileApi, 
        {
          method: 'GET', // or GET
          headers:{
            'Authorization':'Token '+ Storedtoken,
            'Content-Type': 'application/json'
          },
          mode: 'no-cors',
          cache: 'default'
      })
      .then(res => res.json())
      .then(res => {
        
        let theResult=res.result;

        console.log("Employee Profile Result: ", theResult);
  
        let name;
        
        JSON.parse(JSON.stringify(theResult),(key,value)=>
        {
            if (key === 'name') {
              console.log(value);
              localStorage.setItem('Emp_name', value);
            }
          
            if (key === 'sn') {
            
              console.log("SN: ",value);
              localStorage.setItem('Emp_serial', value);
  
            }
  
            if (key === 'enterprise') {
            
              console.log("Enterprise name: ", value);
              localStorage.setItem('Emp_enterprise', value);
  
            }
            if (key === 'location') {
            
              console.log("location: ", value);
              localStorage.setItem('Emp_location', value);
  
            }
            if (key === 'uid') {
            
              console.log("uid: ", value);
              localStorage.setItem('Emp_uid', value);
  
            }

            if (key === 'user_name') {
            
              console.log("user_name : ", value);
              localStorage.setItem('Emp_username', value);
  
            }
            if (key === 'gender') {
            
              console.log("gender: ", value);
              localStorage.setItem('Emp_gender', value);
  
            }
            
            
  
        });
        
      
      })

      //---------------------------------------------

      // Get advisor profile

      fetch(URL_adProfileApi, 
        {
          method: 'GET', // or GET
          headers:{
            'Authorization':'Token '+ Storedtoken,
            'Content-Type': 'application/json'
          },
          mode: 'cors',
          cache: 'default'
      })
      .then(res => res.json())
      .then(res => {
        
        let theResult=res.result;

        console.log("Advisor Profile Result: ", theResult);
  
        let name;
        
        JSON.parse(JSON.stringify(theResult),(key,value)=>
        {
            if (key === 'name') {
              console.log(value);
              localStorage.setItem('Emp_name', value);
            }
          
            if (key === 'sn') {
            
              console.log("SN: ",value);
              localStorage.setItem('Emp_serial', value);
  
            }
  
            if (key === 'enterprise') {
            
              console.log("Enterprise name: ", value);
              localStorage.setItem('Emp_enterprise', value);
  
            }
            if (key === 'location') {
            
              console.log("location: ", value);
              localStorage.setItem('Emp_location', value);
  
            }
            if (key === 'uid') {
            
              console.log("uid: ", value);
              localStorage.setItem('Emp_uid', value);
  
            }

            if (key === 'user_name') {
            
              console.log("user_name : ", value);
              localStorage.setItem('Emp_username', value);
  
            }
            if (key === 'gender') {
            
              console.log("gender: ", value);
              localStorage.setItem('Emp_gender', value);
  
            }
            
            
  
        });
        
      
      })

      //---------------------------------------------


    // Get Wealth advise

    fetch(URL_WealthAdviseApi, 
      {
        method: 'GET', // or GET
        headers:{
          'Authorization':'Token '+ Storedtoken,
          'Content-Type': 'application/json'
        },
        mode: 'cors',
        cache: 'default'
    })
    .then(res => res.json())
    .then(res => {
      
      let theResult=res.result;
      let count = res.count;

      
      console.log("the Count: ", count);
      console.log("the Result: ", theResult);

     




    // FIRST WEALTH ADVISE -----------------------------------------------------------------

      // Getting the first array in the result
      console.log("New Data 1 ", theResult[1]);
     
      // Getting the sub data in first array and reading data by keys
      console.log("Name = ", theResult[1][0].name);
      localStorage.setItem('w1_name', theResult[1][0].name);

      console.log("Symbol = ", theResult[1][0].symbol);
      localStorage.setItem('w1_symbol', theResult[1][0].symbol);

      console.log("Ror = ", theResult[1][0].ror);
      localStorage.setItem('w1_ror', theResult[1][0].ror);

      console.log("Duration = ", theResult[1][0].duration);
      localStorage.setItem('w1_duration', theResult[1][0].duration);

      console.log("Risk = ", theResult[1][0].risk);
      localStorage.setItem('w1_risk', theResult[1][0].risk);

      console.log("Description = ", theResult[1][0].description);
      localStorage.setItem('w1_desc', theResult[1][0].description);

      console.log("Rating = ", theResult[1][0].rating);
      localStorage.setItem('w1_rating', theResult[1][0].rating);

    // 2ND WEALTH ADVISE --------------------------------------------------------

     // Getting the first array in the result
     console.log("New Data 2 ", theResult[2]);
     
     // Getting the sub data in first array and reading data by keys
     console.log("Name = ", theResult[2][0].name);
     localStorage.setItem('w2_name', theResult[2][0].name);

     console.log("Symbol = ", theResult[2][0].symbol);
     localStorage.setItem('w2_symbol', theResult[2][0].symbol);

     console.log("Ror = ", theResult[2][0].ror);
     localStorage.setItem('w2_ror', theResult[2][0].ror);

     console.log("Duration = ", theResult[2][0].duration);
     localStorage.setItem('w2_duration', theResult[2][0].duration);

     console.log("Risk = ", theResult[2][0].risk);
     localStorage.setItem('w2_risk', theResult[2][0].risk);

     console.log("Description 2 = ", theResult[2][0].description);
      localStorage.setItem('w2_desc', theResult[2][0].description);

      console.log("Rating = ", theResult[2][0].rating);
      localStorage.setItem('w2_rating', theResult[2][0].rating);


     // 3RD WEALTH ADVISE -----------------------------------------------------------

          // Getting the first array in the result
          console.log("New Data 3 ", theResult[3]);
     
          // Getting the sub data in first array and reading data by keys
          console.log("Name = ", theResult[3][0].name);
          localStorage.setItem('w3_name', theResult[3][0].name);
    
          console.log("Symbol = ", theResult[3][0].symbol);
          localStorage.setItem('w3_symbol', theResult[3][0].symbol);
    
          console.log("Ror = ", theResult[3][0].ror);
          localStorage.setItem('w3_ror', theResult[3][0].ror);
    
          console.log("Duration = ", theResult[3][0].duration);
          localStorage.setItem('w3_duration', theResult[3][0].duration);
    
          console.log("Risk = ", theResult[3][0].risk);
          localStorage.setItem('w3_risk', theResult[3][0].risk);

          console.log("Description = ", theResult[3][0].description);
          localStorage.setItem('w3_desc', theResult[3][0].description);

          console.log("Rating = ", theResult[3][0].rating);
          localStorage.setItem('w3_rating', theResult[3][0].rating);


    // 4TH WEALTH ADVISE --------------------------------------------------------

     // Getting the first array in the result
     console.log("New Data 4 ", theResult[4]);
     
     // Getting the sub data in first array and reading data by keys
     console.log("Name = ", theResult[4][0].name);
     localStorage.setItem('w4_name', theResult[4][0].name);

     console.log("Symbol = ", theResult[4][0].symbol);
     localStorage.setItem('w4_symbol', theResult[4][0].symbol);

     console.log("Ror = ", theResult[4][0].ror);
     localStorage.setItem('w4_ror', theResult[4][0].ror);

     console.log("Duration = ", theResult[4][0].duration);
     localStorage.setItem('w4_duration', theResult[4][0].duration);

     console.log("Risk = ", theResult[4][0].risk);
     localStorage.setItem('w4_risk', theResult[4][0].risk);

     console.log("Risk = ", theResult[4][0].risk);
     localStorage.setItem('w4_risk', theResult[4][0].risk);

     console.log("Description = ", theResult[4][0].description);
     localStorage.setItem('w4_desc', theResult[4][0].description);

     console.log("Rating = ", theResult[4][0].rating);
      localStorage.setItem('w4_rating', theResult[2][0].rating);


     // 5TH WEALTH ADVISE ----------------------------------------------------

          // Getting the first array in the result
          console.log("New Data 5 ", theResult[5]);
     
          // Getting the sub data in first array and reading data by keys
          console.log("Name = ", theResult[5][0].name);
          localStorage.setItem('w5_name', theResult[5][0].name);
    
          console.log("Symbol = ", theResult[5][0].symbol);
          localStorage.setItem('w5_symbol', theResult[5][0].symbol);
    
          console.log("Ror = ", theResult[5][0].ror);
          localStorage.setItem('w5_ror', theResult[5][0].ror);
    
          console.log("Duration = ", theResult[5][0].duration);
          localStorage.setItem('w5_duration', theResult[5][0].duration);
    
          console.log("Risk = ", theResult[5][0].risk);
          localStorage.setItem('w5_risk', theResult[5][0].risk);

          console.log("Description = ", theResult[5][0].description);
          localStorage.setItem('w5_desc', theResult[5][0].description);

          console.log("Rating = ", theResult[5][0].rating);
        localStorage.setItem('w5_rating', theResult[5][0].rating);

    // 6TH WEALTH ADVISE -----------------------------------------------------

     // Getting the first array in the result
      console.log("New Data 6 ", theResult[6]);
     
      // Getting the sub data in first array and reading data by keys
      console.log("Name = ", theResult[6][0].name);
      localStorage.setItem('w6_name', theResult[6][0].name);

      console.log("Symbol = ", theResult[6][0].symbol);
      localStorage.setItem('w6_symbol', theResult[6][0].symbol);

      console.log("Ror = ", theResult[6][0].ror);
      localStorage.setItem('w6_ror', theResult[6][0].ror);

      console.log("Duration = ", theResult[6][0].duration);
      localStorage.setItem('w6_duration', theResult[6][0].duration);

      console.log("Risk = ", theResult[6][0].risk);
      localStorage.setItem('w6_risk', theResult[6][0].risk);

      console.log("Description = ", theResult[6][0].description);
      localStorage.setItem('w6_desc', theResult[6][0].description);




      
      /*
      JSON.parse(JSON.stringify(theResult),(key,value)=>
      {

            if (key === 'name') {
              console.log(value);
             
             
              localStorage.setItem('w1_name', value);
            }
          
            if (key === 'symbol') {
            
              console.log("Symbol: ",value);
              localStorage.setItem('w1_symbol', value);
  
            }
  
            if (key === 'startdate') {
            
              console.log("StartDate: ", value);
              localStorage.setItem('w1_Sdate', value);
  
            }
            if (key === 'enddate') {
            
              console.log("EndDate: ", value);
              localStorage.setItem('w1_Edate', value);
  
            }
            if (key === 'risk') {
            
              console.log("risk: ", value);
              localStorage.setItem('w1_risk', value);
  
            }
            if (key === 'endbal') {
            
              console.log("EndBal: ", value);
              localStorage.setItem('w1_endBal', value);
  
            }
            if (key === 'rateofreturn') {
            
              console.log("Rate of Return: ", value);
              localStorage.setItem('w1_ror', value);
  
            }
            if (key === 'symbol') {
            
              console.log("Symbol: ", value);
              localStorage.setItem('w1_symbol', value);
  
            }
           

      });
      */
     
    
    })



  });


    
  }  
    







    checkStatus (res) 
    {
      if (res.status >= 200 && res.status < 300) 
      {
        console.log("Login successful")
        return res
      } 
      else 
      {
        let err = new Error(res.statusText)
        err.response = res
        throw err
      }
    }

  /* 
  handleChange = event => {
    this.setState({ email: event.target.value });
    console.log("Print Email ",username);
  };

  handleChange= event => {
    this.setState({ password: event.target.value });
    console.log("Print Password ",password);
  };
  */

 

  render() {

    if(this.state.error) {
      // Some error was thrown. Let's display something helpful to the user
      return (
        <div>
          <h5>Sorry. More than five characters!</h5>
          <details style={{ whiteSpace: 'pre-wrap' }}>
            {this.state.info.componentStack}
          </details>
        </div>
      );
    }



    return (
      <div className="body-inner">
        <div className="card card-white">
          <div className="card-content">

            <section className="logo text-center">
              <h1><a href="#/">{this.state.brand}</a></h1>
            </section>

            <form className="form-horizontal">
              <fieldset>
                <div className="form-group">
                  <TextField
                    label="Email"
                    type="email"
                    value={this.state.email}
                    onChange={this.handleChange}
                    fullWidth
                  />
                </div>
                <div className="form-group">
                  <TextField
                    label="Password"
                    type="password"
                    value={this.state.password}
                    onChange={this.handleChange}
                    className="mt-3"
                    fullWidth
                  />
                </div>
              </fieldset>
            </form>
          </div>
          <div className="card-action border-0 text-right">
          
          <a href="#/app/dashboard" onclick={this.login()} className="color-primary">Login </a>

          </div>
        </div>

        <div className="additional-info">
          <a href={DEMO.signUp}>Sign up</a>
          <span className="divider-h" />
          <a href={DEMO.forgotPassword}>Forgot your password?</a>
        </div>

      </div>
    );
  }

}





const Page = () => (
  <div className="page-login">
    <div className="main-body">
      <QueueAnim type="bottom" className="ui-animate">
        <div key="1">
          <Login />
        </div>
      </QueueAnim>
    </div>
  </div>
);

export default Page;
