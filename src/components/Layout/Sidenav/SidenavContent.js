import React from "react";
import { withRouter } from "react-router-dom";
import Button from "@material-ui/core/Button";
import $ from "jquery";
import "jquery-slimscroll/jquery.slimscroll.min";
//import DEMO from 'constants/demoData';

class SidebarContent extends React.Component {
  constructor() {
    super();
    this.state = {
      error: "",
      adFirms: [],
      click: false
    };
  }

  componentDidMount() {
    const { history } = this.props;
    const nav = this.nav;
    const $nav = $(nav);

    // scroll
    $nav.slimscroll({
      height: "100%"
    });

    // Append icon to submenu
    $nav
      .find(".prepend-icon")
      .prepend('<i class="material-icons">keyboard_arrow_right</i>');

    // AccordionNav
    const slideTime = 250;
    const $lists = $nav.find("ul").parent("li");
    $lists.append('<i class="material-icons icon-has-ul">arrow_drop_down</i>');
    const $As = $lists.children("a");

    // Disable A link that has ul
    $As.on("click", event => event.preventDefault());

    // Accordion nav
    $nav.on("click", e => {
      const target = e.target;
      const $parentLi = $(target).closest("li"); // closest, insead of parent, so it still works when click on i icons
      if (!$parentLi.length) return; // return if doesn't click on li
      const $subUl = $parentLi.children("ul");

      // let depth = $subUl.parents().length; // but some li has no sub ul, so...
      const depth = $parentLi.parents().length + 1;

      // filter out all elements (except target) at current depth or greater
      const allAtDepth = $nav.find("ul").filter(function() {
        if ($(this).parents().length >= depth && this !== $subUl.get(0)) {
          return true;
        }
        return false;
      });
      allAtDepth
        .slideUp(slideTime)
        .closest("li")
        .removeClass("open");

      // Toggle target
      if ($parentLi.has("ul").length) {
        $parentLi.toggleClass("open");
      }
      $subUl.stop().slideToggle(slideTime);
    });

    // HighlightActiveItems
    const $links = $nav.find("a");
    const currentLocation = history.location;
    function highlightActive(pathname) {
      const path = `#${pathname}`;

      $links.each((i, link) => {
        const $link = $(link);
        const $li = $link.parent("li");
        const href = $link.attr("href");
        // console.log(href);

        if ($li.hasClass("active")) {
          $li.removeClass("active");
        }
        if (path.indexOf(href) === 0) {
          $li.addClass("active");
        }
      });
    }
    highlightActive(currentLocation.pathname);
    history.listen(location => {
      highlightActive(location.pathname);
    });

    //Get all the Enteprise or Advisory Firms
    fetch(
      "https://alefone.net/api/v1/enterprise/profile?alias=ao&?all=true&role=advisor",
      {
        method: "GET", // or GET
        headers: {
          authorization: "Token " + localStorage.getItem("currentToken"),
          "Content-Type": "application/json"
        },
        mode: "cors"
      }
    )
      .then(res => res.json())
      .then(res => {
        const result = [].concat(...res.result);
        const newAdFirms = result.map(data => ({
          name: data.name.toUpperCase(),
          alias: data.alias,
          Oid: data.oid,
          status: data.status
        }));

        this.setState({
          adFirms: [...this.state.adFirms, ...newAdFirms]
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    const { adFirms } = this.state;

    return (
      <ul
        className="nav"
        ref={c => {
          this.nav = c;
        }}
      >
        <li className="nav-header">
          <span />
        </li>
        {/* <li><Button href="#/app/dashboard"><i className="nav-icon material-icons">dashboard</i><span className="nav-text">Dashboard</span></Button></li> */}
        <li>
          <Button href="#/app/wdashboard">
            <i className="nav-icon material-icons">content_copy</i>
            <span className="nav-text">Mutual Funds</span>
          </Button>
        </li>
        <li>
          <Button href="#/app/equity">
            <i className="nav-icon material-icons">pie_chart_outlined</i>
            <span className="nav-text">Equity </span>
          </Button>
        </li>
        <li className="nav-divider" />
        <li>
          <Button href="#/app/extra">
            <i className="nav-icon material-icons">person_outline</i>
            <span className="nav-text">Account</span>
          </Button>
          <ul>
            <li>
              <Button className="prepend-icon" href="#/app/page/about">
                <span>Profile</span>
              </Button>
            </li>
          </ul>
        </li>

        <li>
          <Button href="#/app/extra">
            <i className="nav-icon material-icons">person_outline</i>
            <span className="nav-text">Advisory</span>
          </Button>
          <ul>
            {adFirms.map(firms => (
              <li key={firms.Oid}>
                <Button
                  className="prepend-icon"
                  href={`#/enterprise/users?entid=${firms.alias}`}
                >
                  <span>{firms.name}</span>
                </Button>
              </li>
            ))}
          </ul>
        </li>
      </ul>
    );
  }
}

export default withRouter(SidebarContent);
