import React from 'react';
import { Route } from 'react-router-dom';
import loadable from 'react-loadable';
import Header from 'components/Layout/Header';
import Sidenav from 'components/Layout/Sidenav';
import Footer from 'components/Layout/Footer';
import LoadingComponent from 'components/Loading';

let Advisors = loadable({
    loader: () => import('routes/app/routes/advisorsdashboard/'),
    loading: LoadingComponent
  })

  class Enterprise extends React.Component {

    render() {
      const { match } = this.props;
  
      return (
        <div className="main-app-container">
          <Sidenav />
  
          <section id="page-container" className="app-page-container">
            <Header />
  
            <div className="app-content-wrapper">
              <div className="app-content">
                <div className="h-100">
                 <Route path={`${match.url}/users`} component={Advisors} />
                </div>
              </div>
              <Footer />
            </div>
          </section>
  
  
        </div>
      );
    }
  }
  
  export default Enterprise;
  